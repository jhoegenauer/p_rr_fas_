//===========================================================
// util_leaflet.js - leaflet utility module
//
// JS DEPENDANCIES:
// * leaflet 1.x
// * jquery
// * bootstrap and util_ui required for:
//     - L.Control.BasePicker
//     - L.Control.ButtonPanel
//     - L.Control.CenterPanel
//     - L.cwis popup formatting (not required but makes nicer)
//     - L.nwsWatches popup formatting (util_ui required, bootstrap makes nicer)
//
// DATA DEPENDENCIES
// * USGS TX-WSC Search API ( https://txpub.usgs.gov/dss/search_api/ )
// * USGS TX-WSC GWIS       ( https://txpub.usgs.gov/dss/gwis/ )
// * USGS TX-WSC CWIS       ( unreleased )
// * many 3rd party map services (basemaps, weather layers, etc.)
//
// written by: Joe Vrabel, USGS TX-WSC Austin (jvrabel@usgs.gov)
// uses modified open source plugins from other authors (original sources noted in un-minified code where applicable)
//===========================================================
//"use strict";

// check dependencies
if (window.L===undefined) {
    console.warn("[util_leaflet] leaflet must be loaded - util_leaflet may not function correctly");
} else if ( parseFloat(L.version)<1 ) {
    console.warn("[util_leaflet] leaflet version 1+ required - util_leaflet may not function correctly");
}
if (window.jQuery===undefined) {
    console.warn("[util_leaflet] jQuery must be loaded - util_leaflet may not function correctly");
}

// create module object - all utils use same object
if (util === undefined) { var util = {}; }

// set script path
util.path_util_leaflet = $("script[src]").last().attr("src").split('?')[0].split('/').slice(0,-1).join('/')+'/';

// IE 'no transport' for ajax requests
if($ && $.support){ $.support.cors=true; }


//===========================================================
// MAP OPTIONS
//===========================================================

util.mapOpts = {
    preferCanvas       : false,   // [Boolean, false] Whether Paths should be rendered on a Canvas renderer (default is SVG)
    
    // ...control options...
    attributionControl : false,   // [Boolean, true ] whether the attribution control is added to the map by default
    zoomControl        : false,   // [Boolean, true ] whether the zoom control is added to the map by default
    
    // ...interaction options...
    closePopupOnClick  : true,    // [Boolean, true ] set it to false if you don't want popups to close when user clicks the map
    zoomSnap           : 1,       // [Number,  1    ] forces the map's zoom level to always be a multiple of this
    zoomDelta          : 1,       // [Number,  1    ] how much the map's zoom level will change after a zoomIn(), zoomOut(), etc
    trackResize        : true,    // [Boolean, true ] whether the map automatically handles browser window resize to update itself
    boxZoom            : true,    // [Boolean, true ] whether the map can be zoomed to a rectangular area specified by dragging the mouse while pressing shift
    doubleClickZoom    : true,    // [Boolean, true ] whether the map can be zoomed in by double clicking on it and zoomed out by double clicking while holding shift. If passed 'center', double-click zoom will zoom to the center of the view regardless of where the mouse was
    dragging           : true,    // [Boolean, true ] whether the map be draggable with mouse/touch or not
    
    // ...map state options...
    crs       : L.CRS.EPSG3857,   // [CRS, L.CRS.EPSG3857] Coordinate Reference System to use - don't change this if you're not sure what it means
    center    : [39,-97],         // [LatLng,        null] initial geographical center of the map
    zoom      : 4,                // [Number,        null] initial map zoom
    minZoom   : 3,                // [Number,        null] minimum zoom level of the map. Overrides any minZoom set on map layers
    maxZoom   : 18,               // [Number,        null] maximum zoom level of the map. This overrides any maxZoom set on map layers
    layers    : [],               // [Layer[],       null] initial layers to add to the map
    maxBounds : null,             // [LatLngBounds,  null] when set, map restricted to the given geographical bounds, bouncing the user back when he tries to pan outside
    //renderer: XXX               // [Renderer,   depends] default method for drawing vector layers on the map - L.SVG or L.Canvas by default depending on browser support.
    
    // ...animation options...
    //zoomAnimation        : XXX, // [Boolean, depends] whether the tile zoom animation is enabled. By default it's enabled in all browsers that support CSS3 Transitions except Android
    zoomAnimationThreshold : 4,   // [Number,  4      ] won't animate zoom if the zoom difference exceeds this value
    //fadeAnimation        : XXX, // [Boolean, depends] whether the tile fade animation is enabled. By default it's enabled in all browsers that support CSS3 Transitions except Android
    //markerZoomAnimation  : XXX, // [Boolean, depends] whether markers animate their zoom with the zoom animation, if disabled they will disappear for the length of the animation. By default it's enabled in all browsers that support CSS3 Transitions except Android
    //transform3DLimit     : XXX, // [Number,  2^23   ] maximum size of a CSS translation transform - normally should not be changed
    
    // ...panning inertia options...
    //inertia           : XXX,      // [Boolean, depends ] if enabled, panning of the map will have an inertia effect where the map builds momentum while dragging and continues moving in the same direction for some time. Feels especially nice on touch devices
    inertiaDeceleration : 3000,     // [Number,  3000    ] the rate with which the inertial movement slows down, in pixels/second2
    inertiaMaxSpeed     : Infinity, // [Number,  Infinity] max speed of the inertial movement, in pixels/second
    easeLinearity       : 0.2,      // [Number,  0.2     ] N/A
    worldCopyJump       : true,     // [Boolean, false   ] with this option enabled, the map tracks when you pan to another "copy" of the world and seamlessly jumps to the original one so that all overlays like markers and vector layers are still visible
    maxBoundsViscosity  : 0.0,      // [Number,  0.0     ] if maxBounds is set, this option will control how solid the bounds are when dragging the map around
    
    // ...keyboard navigation options...
    keyboard         : true, // [Boolean, true] makes the map focusable and allows users to navigate the map with keyboard arrows and +/- keys
    keyboardPanDelta : 80,   // [Number,  80  ] amount of pixels to pan when pressing an arrow key
    
    // ...mousewheel options...
    scrollWheelZoom     : true, // [Boolean, true] whether the map can be zoomed by using the mouse wheel. If passed 'center', it will zoom to the center of the view regardless of where the mouse was
    wheelDebounceTime   : 40,   // [Number,  40  ] limits the rate at which a wheel can fire (in milliseconds)
    wheelPxPerZoomLevel : 60,   // [Number,  60  ] how many scroll pixels mean a change of one full zoom level (smaller values will make wheel-zooming faster and vice versa)
    
    // ...touch interaction options...
    tap                : true, // [Boolean, true   ] enables mobile hacks for supporting instant taps (fixing 200ms click delay on iOS/Android) and touch holds (fired as contextmenu events) Boolean true    Enables mobile hacks for supporting instant taps (fixing 200ms click delay on iOS/Android) and touch holds (fired as contextmenu events).
    tapTolerance       : 15,   // [Number,  15     ] the max number of pixels a user can shift his finger during touch for it to be considered a valid tap
    //touchZoom        : XXX,  // [Boolean, depends] whether the map can be zoomed by touch-dragging with two fingers
    bounceAtZoomLimits : true  // [Boolean, true   ] set it to false if you don't want the map to zoom beyond min/max zoom and then bounce back when pinch-zooming
    
}; // mapOpts


//===========================================================
// MAP CONTROLS
//
// usage:
//
// (1) leaflet map option extension: add control to map when map is created
//     map option can be boolean true to add using default options or an object of control options to use
//     only a single control of the same type can be added to the same map using this method
//
//     L.map( <String> id, {
//         <Leaflet Map options>?,
//         controlnameControl: <Boolean> true_false | {<control options>}
//     });
//
// (2) leaflet control extension: create control which can be added to map after map creation
//     multiple controls of the same type can be added to the same map using this method
//
//     controlnameControl = L.control.controlname( <control options>? )
//     L.control.controlname( <control options>? ).addTo( <map> map );
//
// (3) util.addControl
//     multiple controls of the same type can be added to the same map using this method
//
//     util.addControl( <String> controlname, <control options>? );
//
//
// where
//   'controlname' is the name of the control, eg: 'zoomhome'
//   <control options> is object of options defined in the control constructor
//
// many controls have user methods - see specific control for details
//
// each control added (all methods) can be accessed from the map.controls object
// each is named the controlname with an increment counter starting at 1
// example:
//
//    L.control.coordinates().addTo(map); // 1st added, accessible by map.controls.coordinates1
//    L.control.coordinates().addTo(map); // 2nd added, accessible by map.controls.coordinates2
//    map.controls.coordinates1.remove(); // access 2nd one and do something with it
//
//===========================================================

//-----------------------------------------------------------
// util.addControl
//   convenience function to add util controls
//   if map not supplied as an input option, uses the util default map last set
//   if no default map set, error
//   when map supplied as an option sets the util default map
//
// usage:
//    util.addControl( <String> controlname, <control options>? );
// where
//    'controlname' is the control name (lowercase)
//    <control options> are the (optional) control options
//    options must contain "map" if util.map not set yet
//-----------------------------------------------------------
util.addControl = function ( name, options ) {
    name = name.toLowerCase();
    var funcName = "util [addControl]: "+name;
    console.log(funcName + "");
    
    // get the map to use and set util default map
    if (options     === undefined) { options  = {}; }
    if (options.map !== undefined) { util.map = options.map; }
    if ( util.map === undefined) {
        console.warn( "map not specified in options and there is no default map set - cannot add control" );
        return 1;
    }
    // add control to map
    L.control[name](options).addTo( util.map );
};

//-----------------------------------------------------------
// L.Control.ZoomHome
//   add extended zoom control with additional buttons
//
// control has these methods:
//   zoomhomeControl.setZoomHomeTarget( <LatLngBounds>|<LatLng>|<Array>|<Function>? ); // set zoomHomeTarget (bounds, point, array equivalents of these, or function) for home   button, sets current map bounds if no input provided
//   zoomhomeControl.setZoomPtsTarget(  <LatLngBounds>|<LatLng>|<Array>|<Function>? ); // set zoomPtsTarget  (bounds, point, array equivalents of these, or function) for points button, sets current map bounds if no input provided
//-----------------------------------------------------------

// constructor
// adapted from: https://github.com/alanshaw/leaflet-zoom-min
L.Control.ZoomHome = L.Control.Zoom.extend({
    
    //------------------------------
    // public
    //------------------------------

    //.............................
    // creation options
    options : {
        position       : "topleft",        // position on map, "topleft", "topright", "bottomleft", "bottomright"
        
        // buttons
        //   array containing strings "in", "home", "pts", "geo", and/or "out"
        //   can have any number, order in array sets button order in widget
        buttons        : ["in","out"],            // default - just in and out
        
        zoomInText     : "+",                     // zoom in button text
        zoomInTitle    : "Zoom in a level",       // zoom in button title (hover tooltip)
        
        zoomHomeText   : undefined,               // zoom home button text (world image used when omitted)
        zoomHomeTitle  : "Zoom home",             // zoom home button title (hover tooltip)
        zoomHomeTarget : undefined,               // see below
        onZoomHome     : undefined,               // function to execute when zoom home button clicked
        
        zoomPtsText    : undefined,               // zoom points button text (dots image used when omitted)
        zoomPtsTitle   : "Zoom to points",        // zoom points button title (hover tooltip)
        zoomPtsTarget  : undefined,               // see below
        onZoomPts      : undefined,               // function to execute when zoom points button clicked
        
        zoomGeoText    : undefined,               // zoom geolocate button text (target image used when omitted)
        zoomGeoTitle   : "Zoom to your location", // zoom geolocate button title (hover tooltip)
        zoomGeoTarget  : undefined,               // default zooms map to your location, can set to a function that takes map as input to override this
        onZoomGeo      : undefined,               // function to execute when zoom geo button clicked

        zoomOutText    : "-",                     // zoom out button text
        zoomOutTitle   : "Zoom out a level",      // zoom out button title (hover tooltip)
        
        // zoomHomeTarget, zoomPtsTarget
        //   one of the following:
        //     (1) L.latLngBounds - clicking button zooms map to these bounds (pan and zoom)
        //     (2) L.LatLng       - clicking button centers map at this point and...
        //                              zoomHomeTarget - zooms out to farthest extent
        //                              zoomPtsTarget  - zooms in  to closest  extent
        //     (3) function       - clicking button executes specified function, function's 1st and only argument accesses the map
        //   if none of the above set, clicking the button does nothing
        //   example bounds input:
        //     [[40.712, -74.227],[40.774, -74.125]]
        //     L.latLngBounds([[40.712, -74.227],[40.774, -74.125]])
        //     L.latLngBounds( L.latLng(40.712, -74.227), L.latLng(40.774, -74.125) )
        //   example point input:
        //     [30,-90]
        //     L.latLng(30,-90)
        //     L.latLng([30,-90])
        //   example function input:
        //     function() { /* do something */ }
        //     function(map){ map.fitWorld() }
    },
    
    //.............................
    // methods
    
    // setZoomHomeTarget
    setZoomHomeTarget: function(target) {
        var that = this;
        if (target !== undefined) {
            // set input
            that.options.zoomHomeTarget = target; 
        } else {
            // set current map extent after delay
            setTimeout( function() {
                that.options.zoomHomeTarget = that._map.getBounds();
            }, 1000);
        }
    },
    
    // setZoomPtsTarget
    setZoomPtsTarget: function(target) {
        var that = this;
        if (target !== undefined) {
            // set input
            that.options.zoomPtsTarget = target;
        } else {
            // set current map extent after delay
            setTimeout( function() {
                that.options.zoomPtsTarget = that._map.getBounds();
            }, 1000);
        }
    },
    
    //------------------------------
    // private
    //------------------------------
    
    onAdd : function (map) {
        var container = L.DomUtil.create("div", "leaflet-control-zoom leaflet-bar");
        var options   = this.options;
        this._map     = map;
        var that      = this;
        
        // functions to create buttons
        var createIn   = function(that) { that._zoomInButton   = that._createButton( options.zoomInText,   options.zoomInTitle,                           "leaflet-control-zoom-in",                              container, that._zoomIn,   that ); };
        var createHome = function(that) { that._zoomHomeButton = that._createButton( options.zoomHomeText, options.zoomHomeTitle, (options.zoomHomeText ? "leaflet-control-zoom-in":"leaflet-control-zoom-home"), container, that._zoomHome, that ); };
        var createPts  = function(that) { that._zoomPtsButton  = that._createButton( options.zoomPtsText,  options.zoomPtsTitle,  (options.zoomPtsText  ? "leaflet-control-zoom-in":"leaflet-control-zoom-pts" ), container, that._zoomPts,  that ); };
        var createGeo  = function(that) { that._zoomGeoButton  = that._createButton( options.zoomGeoText,  options.zoomGeoTitle,  (options.zoomGeoText  ? "leaflet-control-zoom-in":"leaflet-control-zoom-geo" ), container, that._zoomGeo,  that ); };
        var createOut  = function(that) { that._zoomOutButton  = that._createButton( options.zoomOutText,  options.zoomOutTitle,                          "leaflet-control-zoom-out",                             container, that._zoomOut,  that ); };
        
        // create specified buttons in specified order
        for (var i=0; i<options.buttons.length; i++) { 
            switch( options.buttons[i].toLowerCase() ) {
                case "in":
                    createIn(that);
                    break;
                case "home":
                    createHome(that);
                    break;
                case "pts":
                    createPts(that);
                    break;
                case "geo":
                    createGeo(that);
                    break;
                case "out":
                    createOut(that);
                    break;
                default:
                    // do nothing
                    break;
            }
        }
        
        // set update function to disable buttons
        this._updateDisabled();
        map.on("zoomend zoomlevelschange", this._updateDisabled, this);

        // add to map.controls and return container
        if (map.controls===undefined) { map.controls={}; }
        var controlN=0; var doneN=false;
        while(!doneN) {
            controlN++;  this._controlID="zoomhome"+controlN;
            if(map.controls[this._controlID]===undefined){ doneN=true; }
        }
        map.controls[this._controlID] = this;
        return container;
    },
    
    onRemove: function(map) {
        // cleanup
        map.off("zoomend zoomlevelschange", this._updateDisabled, this);
        delete map.controls[this._controlID];
    },
    
    // zoom home button callback
    _zoomHome: function () {
        this._zoomTarget( this.options.zoomHomeTarget, "home" ); // zoom to target
        if (typeof this.options.onZoomHome === "function") {     // execute event
            this.options.onZoomHome();
        }
    },
    
    // zoom points button callback
    _zoomPts: function() {
        this._zoomTarget( this.options.zoomPtsTarget, "pts" ); // zoom to target
        if (typeof this.options.onZoomPts === "function") {    // execute event
            this.options.onZoomPts();
        }
    },

    // zoom geo button callback
    _zoomGeo: function() {
        if (typeof this.options.zoomGeoTarget === "function") { // execute user function
            this.options.zoomGeoTarget(this._map);
        } else {
            this._map.locate({ setView:true }); // geolocate
        }
        if (typeof this.options.onZoomGeo === "function") { // execute event
            this.options.onZoomGeo();
        }
    },
    
    // zoom to target
    _zoomTarget: function (target,type) {
        // target is one of the following:
        //   (1) L.LatLngBounds - zoom map to bounds (pan and zoom)
        //   (2) L.LatLng       - center map at point without changing the zoom level (pan only)
        //   (3) function       - execute function, function's 1st and only argument accesses the map
        // if none of the above set, do nothing
        
        // convert [lat,lng] => L.LatLng and [[lat,lng],[lat,lng]] => L.latLngBounds
        if ( L.Util.isArray(target) && target.length===2 ) {
            if ( typeof target[0]==="number" ) {
                try{ target = L.latLng(target);       } catch(e){}
            } else {
                try{ target = L.latLngBounds(target); } catch(e){}
            }
        }
        // take action
        switch( typeof target ) {
            case "object":
                if ( target.toBBoxString!==undefined && target.isValid() ) {
                    // valid L.latLngBounds
                    this._map.stop().fitBounds( target );
                } else if ( target.lat!==undefined ) {
                    // L.LatLng
                    switch( type ) {
                        case "home": this._map.stop().setView( target, this._map.getMinZoom() ); break; // home: center and zoom out
                        case "pts" : this._map.stop().setView( target, this._map.getMaxZoom() ); break; // pts:  center and zoom in
                    }
                }
                break;

            case "function":
                // execute function
                target( this._map );
                break;
                
            default:
                // do nothing
                break;
        }
    },
    
    // update function to disable buttons
    _updateDisabled: function () {
        // zoom in
        if (this._zoomInButton) {
            // button exists
            if (this._map._zoom >= this._map.getMaxZoom()) {
                // zoomed all the way in - disable
                L.DomUtil.addClass(   this._zoomInButton, "leaflet-disabled");
            } else {
                // enable
                L.DomUtil.removeClass(this._zoomInButton, "leaflet-disabled");
            }
        }
        // zoom out
        if (this._zoomOutButton) {
            // button exists
            if (this._map._zoom <= this._map.getMinZoom()) {
                // zoomed all the way out - disable
                L.DomUtil.addClass(   this._zoomOutButton, "leaflet-disabled");
            } else {
                // enable
                L.DomUtil.removeClass(this._zoomOutButton, "leaflet-disabled");
            }
        }
    }
});

// constructor registration
L.control.zoomhome = function(options) {
    return new L.Control.ZoomHome(options);
};

// add new map constructor option to create control
L.Map.mergeOptions({
    zoomControl     : false, // regular zoom  control map option - default off
    zoomhomeControl : false  // new zoom home control map option - default off
});

// add control when map created if map option set
L.Map.addInitHook( function() {
    if (this.options.zoomhomeControl) {
        this.addControl(
            L.control.zoomhome(
                ( this.options.zoomhomeControl===true ? {} : this.options.zoomhomeControl ) // default options when true or specified options object
            )
        );
    }
});


//-----------------------------------------------------------
// L.Control.Coordinates
//   add control showing map scale and/or lat-lon of mouse position over map
//-----------------------------------------------------------

// constructor
L.Control.Coordinates = L.Control.extend({
    
    //------------------------------
    // public
    //------------------------------
        
    //.............................
    // creation options
    options: {
        position     : "bottomleft", // position on map, "topleft", "topright", "bottomleft", "bottomright"
        showScale    : true,         // whether to display map scale
        showLatLon   : true,         // whether to display lat-lon
        latLonFormat : 4             // number of decimals to display for lat and lon, set to string "dms" to display as degree-minute-second
    },
    
    //------------------------------
    // private
    //------------------------------
    
    onAdd: function(map) {
        // set map and create container
        this._map = map;
        this._container = L.DomUtil.create("div","leaflet-control-coordinates");

        // mouse events
        map.on("mousemove zoomend",  this._update,   this);
        map.on("mouseover mouseout", this._showHide, this);

        // init when map ready
        map.whenReady(this._update,   this);
        map.whenReady(this._showHide, this);

        // add to map.controls and return container
        if (map.controls===undefined) { map.controls={}; }
        var controlN=0; var doneN=false;
        while(!doneN) {
            controlN++;  this._controlID="coordinates"+controlN;
            if(map.controls[this._controlID]===undefined){ doneN=true; }
        }
        map.controls[this._controlID] = this;
        return this._container;
    },
    
    onRemove: function(map) {
        // cleanup
        map.off("mousemove zoomend",  this._update,   this);
        map.off("mouseover mouseout", this._showHide, this);
        delete map.controls[this._controlID];
    },
    
    // update text
    _update: function(e) {
        // get scale
        var opts = this.options;
        var s = this._getScale( this._map.getZoom() );
        if (s && opts.showScale) {
            s = "<b>Scale:</b> 1 <b>&colon;</b> "+s+"&nbsp;&nbsp;";
        } else {
            s = "";
        }
        // get lat-lon
        var ll  = "";
        var pos = e.latlng;
        if (pos && opts.showLatLon) {
            ll =
                '<span class="leaflet-control-coordinates-latlon">'   +
                    '<b>Lat:</b> ' + this._fmtCoord( pos.lat ) + '&nbsp;&nbsp;' +
                    '<b>Lon:</b> ' + this._fmtCoord( pos.lng ) +
                '</span>';
        }
        // set text
        this._container.innerHTML = s + ll + '&nbsp;';
    },
    
    // show-hide things when mouse enters-exits map
    _showHide: function(e) {
        var opts = this.options;
        switch(e.type) {
            case "mouseover":
                $(this._container).find(".leaflet-control-coordinates-latlon").stop().fadeIn(200); // show lat-lon
                $(this._container).stop().animate( {opacity:1}, 200);                              // show container
                break;
            case "mouseout":
            default:
                $(this._container).find(".leaflet-control-coordinates-latlon").stop().fadeOut(200); // hide lat-lon
                if (!opts.showScale) { $(this._container).stop().animate( {opacity:0}, 200); }      // hide container if no scale
                break;
        }
    },
    
    // helper: format a coordinate number according to options
    _fmtCoord: function(num) {
        var opts = this.options;
        if ( typeof opts.latLonFormat === "string" ) {
            // convert to DD MM SS
            var sign = ( num<0 ? "-" : "" );
            num = Math.abs(num);
            var DD = Math.floor(num);
            var MM = Math.floor(60*(num-DD));
            var SS = Math.round( (num-DD-MM/60)*3600 );
            return sign + DD + "&deg; " + MM + "' " + SS + '"';
        } else {
            // rounded decimal degrees
            return num.toFixed( opts.latLonFormat );
        }
    },
    
    // helper: convert map zoom level => map scale
    // handles fractional zoom levels by interpolation
    _getScale: function(z) {
        // discete zoom level => scale lookup
        // source: http://gis.stackexchange.com/questions/7430/what-ratio-scales-do-google-maps-zoom-levels-correspond-to
        var z2s = {
            19 :       1128,
            18 :       2257,
            17 :       4514,
            16 :       9028,
            15 :      18056,
            14 :      36112,
            13 :      72224,
            12 :     144448,
            11 :     288895,
            10 :     577791,
             9 :    1155581,
             8 :    2311162,
             7 :    4622325,
             6 :    9244649,
             5 :   18489298,
             4 :   36978597,
             3 :   73957194,
             2 :  147914388,
             1 :  295828775,
             0 :  591657551
        };
        // simple linear interp func
        var interp = function( x1,x2, xi ) {
            var y1 = z2s[x1];
            var y2 = z2s[x2];
            return Math.round( y1 + ((xi-x1)*(y2-y1))/(x2-x1) );
        };
        // get and return scale
        var zi = undefined;
        if ( Math.round(z)===z ) { zi = z2s[z];      } // non-fractional
        else if (z <=  1) { zi = interp(  0, 1, z ); } // (   0,   1 ]
        else if (z <=  2) { zi = interp(  1, 2, z ); } // (   1,   2 ]
        else if (z <=  3) { zi = interp(  2, 3, z ); } //  ...
        else if (z <=  4) { zi = interp(  3, 4, z ); }
        else if (z <=  5) { zi = interp(  4, 5, z ); }
        else if (z <=  6) { zi = interp(  5, 6, z ); }
        else if (z <=  7) { zi = interp(  6, 7, z ); }
        else if (z <=  8) { zi = interp(  7, 8, z ); }
        else if (z <=  9) { zi = interp(  8, 9, z ); }
        else if (z <= 10) { zi = interp(  9,10, z ); }
        else if (z <= 11) { zi = interp( 10,11, z ); }
        else if (z <= 12) { zi = interp( 11,12, z ); }
        else if (z <= 13) { zi = interp( 12,13, z ); }
        else if (z <= 14) { zi = interp( 13,14, z ); }
        else if (z <= 15) { zi = interp( 14,15, z ); }
        else if (z <= 16) { zi = interp( 15,16, z ); }
        else if (z <= 17) { zi = interp( 16,17, z ); } //  ...
        else if (z <= 18) { zi = interp( 17,18, z ); } // (  17,  18 ]
        else if (z <= 19) { zi = interp( 18,19, z ); } // (  18,  19 ]
        return ( zi ? zi.toLocaleString() : zi ); // add commas to thousands
    }
});

// constructor registration
L.control.coordinates = function(options) {
    return new L.Control.Coordinates(options);
};

// add new map constructor option to create control
L.Map.mergeOptions({
    coordinatesControl: false
});

// add control when map created if map option set
L.Map.addInitHook( function() {
    if (this.options.coordinatesControl) {
        this.addControl(
            L.control.coordinates(
                ( this.options.coordinatesControl===true ? {} : this.options.coordinatesControl ) // default options when true or specified options object
            )
        );
    }
});


//-----------------------------------------------------------
// L.Control.InsetMap
//   add inset map control to map
//
// control has these methods:
//   insetmapControl.minimize();                        // close inset map
//   insetmapControl.maximize();                        // open  inset map
//   insetmapControl.changeBaseMap( <String> name    ); // change inset basemap layer by name (see util.layerUrls for supported names)
//   insetmapControl.changeOpacity( <Number> opacity ); // change inset basemap layer opacity (0-1)
//-----------------------------------------------------------

// constructor
// adapted from: https://github.com/Norkart/Leaflet-MiniMap
L.Control.InsetMap = L.Control.extend({
    
    //------------------------------
    // public
    //------------------------------
        
    //.............................
    // creation options
    options: {
        position      : "bottomright",       // position on map: "topleft", "topright", "bottomleft", "bottomright"
        layerName     : "stamen_toner_lite", // inset map layer name, one of the named util layers (see util.layerUrls)
        layerOpacity  : 1,                   // inset map layer opacity [0-1]
        width         : 350,                 // inset map     width  [px]
        height        : 250,                 // inset map     height [px]
        zoomOffset    : -5,                  // offset applied to the zoom in the inset map compared to the zoom of the main map, can be positive or negative
        zoomFixed     : false,               // overrides the offset to apply a fixed zoom level to the inset map regardless of the main map zoom
        zoomAnimation : false,               // whether the inset map should have an animated zoom (true causes lag)
        disableNav    : false,               // whether to disable inset map navigation (pan-zoom)
        toggleButton  : true,                // whether the inset map should have a button to minimize it
        autoToggle    : false,               // whether the inset map should hide automatically if parent map bounds does not fit within the inset map bounds (esp. useful when 'zoomFixed' set)
        buttonWidth   : 30,                  // toggle button width  [px]
        buttonHeight  : 30,                  // toggle button height [px]
        buttonTitle   : "Open or close inset map", // toggle button hover tooltip
        minimize      : true,                // whether to create inset map minimized on creation
        
        // style of the rectangle showing the parent map's current extent (Path.Options object)
        extentRectOptions : { color:"#f80", weight:1, interactive:false },
        
        // style of the shadow rectangle when inset map clicked and dragged (Path.Options object)
        shadowRectOptions : { color:"#000", weight:1, interactive:false }
    },
    
    // close inset map
    minimize: function() {
        if (this.options.toggleButton) {
            this._container.style.width  = this.options.buttonWidth  + "px";
            this._container.style.height = this.options.buttonHeight + "px";
            this._toggleButton.className += (" minimized-" + this.options.position);
        }
        else {
            this._container.style.display = "none";
        }
        this._minimized = true;
        util.changeBaseMap({ map:this._insetMap, name:"none" });
    },
    
    // open inset map
    maximize: function() {
        if (this.options.toggleButton) {
            this._container.style.width  = this.options.width  + "px";
            this._container.style.height = this.options.height + "px";
            this._toggleButton.className = this._toggleButton.className.replace("minimized-"  + this.options.position, "");
        }
        else {
            this._container.style.display = "block";
        }
        this._minimized = false;
        util.changeBaseMap({ map:this._insetMap, name:this.options.layerName, opacity:this.options.layerOpacity });
        // refresh insetmap after opened (get out of sync if main map resized while closed)
        var that = this;
        setTimeout( function(){ that._insetMap.invalidateSize() }, 250 );
    },
    
    // change the inset map layer to one of the named util layers (see util.layerUrls)
    changeBaseMap: function(name) {
        this.options.layerName = (name || "stamen_toner_lite");
        util.changeBaseMap({
            map  : this._insetMap,
            name : this.options.layerName
        });
    },
    
    // change the inset map layer opacity (0-1)
    changeOpacity: function(opacity) {
        this.options.layerOpacity = (opacity || 1);
        util.changeBaseMap({
            map     : this._insetMap,
            name    : "same",
            opacity : this.options.layerOpacity
        });
    },
    
    //------------------------------
    // private
    //------------------------------
    
    onAdd: function(map) {
        // get options
        var options = this.options;
        
        // create container
        this._container = L.DomUtil.create("div", "leaflet-control-insetmap");
        var container = this._container;
        container.style.width  = options.width  + "px";
        container.style.height = options.height + "px";
        
        // prevent events from propagating to main map
        L.DomEvent.disableClickPropagation(container);
        L.DomEvent.on(container, "mousewheel", L.DomEvent.stopPropagation);
        
        // create inset map
        this._insetMap = new L.Map( container, {
            attributionControl : false,
            zoomControl        : false,
            zoomAnimation      : options.zoomAnimation,
            crs                : map.options.crs
        });
        var insetmap = this._insetMap;
        
        // disable zoom if zoomFixed or disableNav
        if (options.zoomFixed || options.disableNav) {
            insetmap.boxZoom.disable();
            insetmap.touchZoom.disable();
            insetmap.doubleClickZoom.disable();
            insetmap.scrollWheelZoom.disable();
        }
        
        // disable more if disableNav
        if (options.disableNav) {
            insetmap.dragging.disable();
            insetmap.keyboard.disable();
            if (insetmap.tap) { insetmap.tap.disable(); }
            $( insetmap.getContainer() ).css({ cursor:"default" });
        }
        
        // add inset map layer
        this.changeBaseMap( options.layerName    );
        this.changeOpacity( options.layerOpacity );
        
        // setup main and inset map communication
        this._mainMap = map;
        insetmap.whenReady(
            L.Util.bind( function() {
                // add rectangles
                this._aimingRect = L.rectangle( map.getBounds(), options.extentRectOptions ).addTo( insetmap );
                this._shadowRect = L.rectangle( map.getBounds(), options.shadowRectOptions ).addTo( insetmap );
                // add events
                     map.on( "moveend",   this._onMainMapMoved,        this );
                     map.on( "move",      this._onMainMapMoving,       this );
                insetmap.on( "movestart", this._onInsetMapMoveStarted, this );
                insetmap.on( "move",      this._onInsetMapMoving,      this );
                insetmap.on( "moveend",   this._onInsetMapMoved,       this );
            }, this )
        );
        
        // add toggle button if specified
        if (options.toggleButton) {
            this._toggleButton = this._createButton( "", this.options.buttonTitle, ("leaflet-control-insetmap-button leaflet-control-insetmap-button-" + this.options.position), this._container, this._toggleButtonClicked, this);
            this._toggleButton.style.width  = this.options.buttonWidth  + "px";
            this._toggleButton.style.height = this.options.buttonHeight + "px";
        }
        
        // collapse on creation if specified
        if ( options.minimize===true ) {
            this.minimize();
        }

        // add to map.controls and return container
        if (map.controls===undefined) { map.controls={}; }
        var controlN=0; var doneN=false;
        while(!doneN) {
            controlN++;  this._controlID="insetmap"+controlN;
            if(map.controls[this._controlID]===undefined){ doneN=true; }
        }
        map.controls[this._controlID] = this;
        return container;
    },
    
    onRemove: function(map) {
        // cleanup
        this._mainMap.off( "moveend",   this._onMainMapMoved,        this);
        this._mainMap.off( "move",      this._onMainMapMoving,       this);
        this._insetMap.off("movestart", this._onInsetMapMoveStarted, this);
        this._insetMap.off("move",      this._onInsetMapMoving,      this);
        this._insetMap.off("moveend",   this._onInsetMapMoved,       this);
        delete map.controls[this._controlID];
    },
    
    addTo: function(map) {
        L.Control.prototype.addTo.call( this, map );
        this._insetMap.setView( this._mainMap.getCenter(), this._decideZoom(true) );
        this._setDisplay( this._decideMinimized() );
        return this;
    },
    
    _createButton: function (html, title, className, container, fn, context) {
        var link = L.DomUtil.create("a", className, container);
        link.innerHTML = html;
        link.href = "#";
        link.title = title;
        var stop = L.DomEvent.stopPropagation;
        L.DomEvent
            .on(link, "click",     stop)
            .on(link, "mousedown", stop)
            .on(link, "dblclick",  stop)
            .on(link, "click",     L.DomEvent.preventDefault)
            .on(link, "click",     fn, context);
        return link;
    },
    _toggleButtonClicked: function () {
        this._userToggled = true;
        if (!this._minimized) {
            this.minimize();
        }
        else {
            this.maximize();
        }
    },
    _setDisplay: function (minimize) {
        if (minimize !== this._minimized) {
            if (!this._minimized) {
                this.minimize();
            }
            else {
                this.maximize();
            }
        }
    },
    _onMainMapMoved: function (e) {
        if (!this._insetMapMoving) {
            this._mainMapMoving = true;
            this._insetMap.setView(this._mainMap.getCenter(), this._decideZoom(true));
            this._setDisplay(this._decideMinimized());
        } else {
            this._insetMapMoving = false;
        }
        this._aimingRect.setBounds(this._mainMap.getBounds());
    },
    _onMainMapMoving: function (e) {
        this._aimingRect.setBounds(this._mainMap.getBounds());
    },
    _onInsetMapMoveStarted:function (e) {
        var lastAimingRect = this._aimingRect.getBounds();
        var sw = this._insetMap.latLngToContainerPoint(lastAimingRect.getSouthWest());
        var ne = this._insetMap.latLngToContainerPoint(lastAimingRect.getNorthEast());
        this._lastAimingRectPosition = {sw:sw,ne:ne};
    },
    _onInsetMapMoving: function (e) {
        if (!this._mainMapMoving && this._lastAimingRectPosition) {
            this._shadowRect.setBounds(new L.LatLngBounds(this._insetMap.containerPointToLatLng(this._lastAimingRectPosition.sw),this._insetMap.containerPointToLatLng(this._lastAimingRectPosition.ne)));
            this._shadowRect.setStyle({opacity:1,fillOpacity:0.3});
        }
    },
    _onInsetMapMoved: function (e) {
        if (!this._mainMapMoving) {
            this._insetMapMoving = true;
            this._mainMap.setView(this._insetMap.getCenter(), this._decideZoom(false));
            this._shadowRect.setStyle({opacity:0,fillOpacity:0});
        } else {
            this._mainMapMoving = false;
        }
    },
    _decideZoom: function (fromMaintoMini) {
        if (!this.options.zoomFixed) {
            if (fromMaintoMini)
                return this._mainMap.getZoom() + this.options.zoomOffset;
            else {
                var currentDiff  = this._insetMap.getZoom() - this._mainMap.getZoom();
                var proposedZoom = this._insetMap.getZoom() - this.options.zoomOffset;
                var toRet;
                if (currentDiff > this.options.zoomOffset && this._mainMap.getZoom() < this._insetMap.getMinZoom() - this.options.zoomOffset) {
                    // inset map zoomed out to min level and cannot zoom any more
                    if (this._insetMap.getZoom() > this._lastInsetMapZoom) {
                        // user trying to zoom in by using the inset map - zoom the main map
                        toRet = this._mainMap.getZoom() + 1;
                        // zoom the inset map out again to keep it visually consistent
                        this._insetMap.setZoom(this._insetMap.getZoom() -1);
                    } else {
                        // either the user is trying to zoom out past the inset map's min zoom or has just panned using it, we can't tell the difference
                        // therefore, we ignore it
                        toRet = this._mainMap.getZoom();
                    }
                } else {
                    // this is what happens in the majority of cases, and always if you configure the min levels + offset in a sane fashion
                    toRet = proposedZoom;
                }
                this._lastInsetMapZoom = this._insetMap.getZoom();
                return toRet;
            }
        } else {
            if (fromMaintoMini) {
                return this.options.zoomFixed;
            } else {
                return this._mainMap.getZoom();
            }
        }
    },
    _decideMinimized: function () {
        if (this._userToggled) {
            return this._minimized;
        }
        if (this.options.autoToggle) {
            if (this._mainMap.getBounds().contains(this._insetMap.getBounds())) {
                return true;
            }
            return false;
        }
        return this._minimized;
    }
});

// constructor registration
L.control.insetmap = function(options) {
    return new L.Control.InsetMap(options);
};

// add new map constructor option to create control
L.Map.mergeOptions({
    insetmapControl: false
});

// add control when map created if map option set
L.Map.addInitHook( function() {
    if (this.options.insetmapControl) {
        this.addControl(
            L.control.insetmap(
                ( this.options.insetmapControl===true ? {} : this.options.insetmapControl ) // default options when true or specified options object
            )
        );
    }
});


//-----------------------------------------------------------
// L.Control.ScaleBar
//   add map scale bar control
//-----------------------------------------------------------

// constructor
L.Control.ScaleBar = L.Control.extend({
    
    //------------------------------
    // public
    //------------------------------
    
    // creation options
    options: {
        position       : "bottomleft", // position on map, "topleft", "topright", "bottomleft", "bottomright"
        maxWidth       : 200,          // max width [px]
        metric         : true,         // whether to show the metric   scale line (m /km)
        imperial       : true,         // whether to show the imperial scale line (mi/ft)
        updateWhenIdle : true          // when true, control updated on moveend, otherwise updated on move (always up-to-date)
    },
    
    //------------------------------
    // private
    //------------------------------
    
    onAdd: function(map) {
        // create scale control
        this._control = new L.Control.Scale( this.options ).addTo( map );

        // add to map.controls and return container
        if (map.controls===undefined) { map.controls={}; }
        var controlN=0; var doneN=false;
        while(!doneN) {
            controlN++;  this._controlID="scalebar"+controlN;
            if(map.controls[this._controlID]===undefined){ doneN=true; }
        }
        map.controls[this._controlID] = this;
        return this._control.getContainer();
    },
    onRemove: function(map) {
        // cleanup
        map.removeControl( this._control );
        delete map.controls[this._controlID];
    }
});

// constructor registration
L.control.scalebar = function(options) {
    return new L.Control.ScaleBar(options);
};

// add new map constructor option to create control
L.Map.mergeOptions({
    scalebarControl: false
});

// add control when map created if map option set
L.Map.addInitHook( function() {
    if (this.options.scalebarControl) {
        this.addControl(
            L.control.scalebar(
                ( this.options.scalebarControl===true ? {} : this.options.scalebarControl ) // default options when true or specified options object
            )
        );
    }
});


//-----------------------------------------------------------
// L.Control.SimpleBasePicker
//   add simple basemap picker control
//   picker is thumb that when clicked toggles between 2 basemaps
//-----------------------------------------------------------

// constructor
L.Control.SimpleBasePicker = L.Control.extend({
    
    //------------------------------
    // public
    //------------------------------
    
    // creation options
    options: {
        position : "topright",     // position on map, "topleft", "topright", "bottomleft", "bottomright"
        name1    : "esri_streets", // basemap 1 name, see util.layerUrls for all available
        name2    : "esri_imagery2" // basemap 2 name, see util.layerUrls for all available
    },
    
    //------------------------------
    // private
    //------------------------------
    
    onAdd: function(map) {
        // create container
        this._container = L.DomUtil.create("div","leaflet-control-simplebasepicker  leaflet-control-simplebasepicker-2");
        
        // set startup basemap and thumb
        var options = this.options;
        setTimeout( function(){
            util.changeBaseMap({
                map  : map,
                name : options.name1
            });
        }, 50);
        $( this._container ).css({
            "background-image": 'url("'+util.path_util_leaflet+'/basethumbs/'+options.name2+'.png")'
        });
        
        // events
        $( this._container )
            .click( function(e){
                // thumb click
                // ...prevent map click evt...
                e.stopPropagation();
                // ...get names based on current class...
                var name_map = ( $(this).is(".leaflet-control-simplebasepicker-1") ? options.name1 : options.name2 );
                var name_thb = ( $(this).is(".leaflet-control-simplebasepicker-2") ? options.name1 : options.name2 );
                // ...change basemap...
                util.changeBaseMap({
                    map  : map,
                    name : name_map
                });
                // ...change thumb img...
                $(this).css({ "background-image" : 'url("'+util.path_util_leaflet+'/basethumbs/'+name_thb+'.png")' });
                // ...toggle thumb classes...
                $(this).toggleClass( "leaflet-control-simplebasepicker-1  leaflet-control-simplebasepicker-2" );
            })
            .dblclick( function(e){
                // prevent map click evt
                e.stopPropagation();
            });
        
        // add to map.controls and return container
        if (map.controls===undefined) { map.controls={}; }
        var controlN=0; var doneN=false;
        while(!doneN) {
            controlN++;  this._controlID="simplebasepicker"+controlN;
            if(map.controls[this._controlID]===undefined){ doneN=true; }
        }
        map.controls[this._controlID] = this;
        return this._container;
    },
    
    onRemove: function(map) {
        // cleanup
        $( this._container ).off("click dblclick");
        delete map.controls[this._controlID];
    }
    
});

// constructor registration
L.control.simplebasepicker = function(options) {
    return new L.Control.SimpleBasePicker(options);
};

// add new map constructor option to create control
L.Map.mergeOptions({
    simplebasepickerControl: false
});

// add control when map created if map option set
L.Map.addInitHook( function() {
    if (this.options.simplebasepickerControl) {
        this.addControl(
            L.control.simplebasepicker(
                ( this.options.simplebasepickerControl===true ? {} : this.options.simplebasepickerControl ) // default options when true or specified options object
            )
        );
    }
});


//-----------------------------------------------------------
// L.Control.BasePicker
//   add basemap picker control
//   picker is button that when clicked opens panel with basemap and/or overlay thumbs
//   one basemap can be selected at a time, multiple overlays can be shown
//
// see public methods below for those available
//
// IMPORTANT: requires bootstrap and util_ui
//-----------------------------------------------------------

// constructor
L.Control.BasePicker = L.Control.extend({
    
    //------------------------------
    // public
    //------------------------------
    
    // creation options
    options: {
        position    : "topright", // position on map, "topleft", "topright", "bottomleft", "bottomright"
        label       : "Base Map", // button label
        buttonWidth : undefined,  // button width [px], omit for auto
        title       : "",         // button hover tooltip

        // basemaps: array of:
        //   name  - the util.layerUrls layer name (required)
        //   label - thumb label (optional, 'name' used if omitted)
        // set empty array for no basemap section in picker
        // the 1st basemap is shown on startup
        basemaps : [
            { name:"esri_topo",     label:"Topographic" },
            { name:"esri_imagery2", label:"Imagery"     },
            { name:"esri_streets",  label:"Streets"     }
        ],
        baseOpacity : 0.8, // startup basemap opacity

        // overlays: array of:
        //   name    - the util.layerUrls layer name (required, must be a reference layer)
        //   label   - thumb label (optional, 'name' used if omitted)
        //   visible - whether to show on startup, true or false (default false)
        // set empty array for no overlay section in picker
        overlays : [],
        overlayOpacity : 1.0 // startup overlay opacity
    },
    
    // open
    open: function(){
        util.togglePanel( "#"+this.idPnl, true );
    },

    // close
    close: function(){
        util.togglePanel( "#"+this.idPnl, false );
    },

    //------------------------------
    // private
    //------------------------------
    
    onAdd: function(map) {
        // create container
        this._container = L.DomUtil.create("div","leaflet-control-basepicker util-ui-unselectable");
        
        // disable map mouse events in container
        L.DomEvent.disableClickPropagation( this._container);
        L.DomEvent.disableScrollPropagation(this._container);

        // set unique button and panel id's
        var r = Math.floor(1000*Math.random());
        this.idBtn = "util_leaflet_basepicker_btn"+r;
        this.idPnl = "util_leaflet_basepicker_pnl"+r;

        // add panel
        var panel = $(
            '<div id="'+this.idPnl+'">'+
                '<div class="well">'+
                    '<p style="margin:0px 0px 5px 0px;">Select a base map:</p>'+
                    '<p style="margin:5px 0px 0px 0px;">'+
                        'Opacity: <input type="hidden" class="slider-basemap slider-input"/> <span class="slider-value label label-primary">'+this.options.baseOpacity.toFixed(2)+'</span>'+
                    '</p>'+
                '</div>'+
                '<div class="well">'+
                    '<p style="margin:0px 0px 5px 0px;">Select one or more overlays:</p>'+
                    '<p style="margin:5px 0px 0px 0px;">'+
                        'Opacity: <input type="hidden" class="slider-overlay slider-input"/> <span class="slider-value label label-primary">'+this.options.overlayOpacity.toFixed(2)+'</span>'+
                    '</p>'+
                '</div>'+
                '<button type="button" class="btn btn-default btn-xs" onclick="util.togglePanel(\'#'+this.idPnl+'\',false);" style="float:right;">Close</button>'+
            '</div>'
        );

        // add basemaps
        $(this.options.basemaps).each( function(idx,o){
            if (o.name===undefined) {
                console.warn("util [BasePicker]: basemap name must be specified - this entry ignored");
                return true;
            }
            panel.find(".well").eq(0).find("p:eq(1)").before(
                '<div style="display:inline-block;" class="thumbnail thumbnail-basemap" data-name="'+o.name+'">'+
                    '<img class="img-responsive" src="'+util.path_util_leaflet+'basethumbs/'+o.name+'.png" />'+
                    '<div class="caption text-center">'+( o.label ? o.label : o.name )+'</div>'+
                '</div>'
            );
        });

        // add overlays
        $(this.options.overlays).each( function(idx,o){
            if (o.name===undefined) {
                console.warn("util [BasePicker]: overlay name must be specified - this entry ignored")
                return true;
            }
            panel.find(".well").eq(1).find("p:eq(1)").before(
                '<div style="display:inline-block;" class="thumbnail thumbnail-overlay" data-name="'+o.name+'">'+
                    '<img class="img-responsive" src="'+util.path_util_leaflet+'basethumbs/'+o.name+'.png" />'+
                    '<div class="caption text-center">'+( o.label ? o.label : o.name )+'</div>'+
                '</div>'
            );
        });

        // add button to container and append panel
        this.options.buttonWidth = ( this.options.buttonWidth===undefined ? "auto" : this.options.buttonWidth+"px" );
        $(this._container).html( '<button id="'+this.idBtn+'" type="button" class="btn btn-primary" title="'+this.options.title+'" style="width:'+this.options.buttonWidth+';">'+this.options.label+'</button>' ).append( panel );

        // add to map.controls and return container
        if (map.controls===undefined) { map.controls={}; }
        var controlN=0; var doneN=false;
        while(!doneN) {
            controlN++;  this._controlID="basepicker"+controlN;
            if(map.controls[this._controlID]===undefined){ doneN=true; }
        }
        map.controls[this._controlID] = this;
        return this._container;
    },
    
    addTo: function(map) {
        L.Control.prototype.addTo.call( this, map );

        // finalize when control container added to map and accessible:

        // create panel
        var position;
        switch(this.options.position.toLowerCase()) {
            case "bottomleft"  :           position=  "up-left" ; break;
            case "bottomright" :           position=  "up-right"; break;
            case "topleft"     :           position="down-left" ; break;
            case "topright"    : default : position="down-right"; break;
        }
        util.createPanel( "#"+this.idBtn, "#"+this.idPnl, "#"+this.idBtn, position);

        // setup basemaps
        var Bthumbs = $(this._container).find(".thumbnail-basemap");
        var Bslider = $(this._container).find(".slider-basemap"   );
        if ( Bthumbs.length>0 ) {
            // update map when thumb clicked
            Bthumbs.click( function() {
                // set selected thumb
                Bthumbs.removeClass("selected");
                $(this).addClass(  "selected");
                // update map layers
                util.changeBaseMap({
                    map     : map,
                    name    : $(this).data("name"),
                    opacity : parseFloat( Bslider.val() )
                });
            });
            // create slider and change opacity when changes
            Bslider.slider({ from:0, to:1, step:0.05, value:this.options.baseOpacity, onChange:function(val){
                // change opacity
                if (map.layers && map.layers.basemapLayerGroup) {
                    map.layers.basemapLayerGroup.eachLayer( function(layer) { layer.setOpacity(parseFloat(val)); });
                }
                // update slider label value
                Bslider.parent().find(".slider-value").html( (100*parseFloat(val)).toFixed(0)+"%" );
            }});
            // show 1st one
            setTimeout( function(){ Bthumbs.eq(0).click(); }, 200 );
        } else {
            // none - remove section
            Bslider.closest(".well").remove();
        }

        // setup overlays
        var Othumbs = $(this._container).find(".thumbnail-overlay");
        var Oslider = $(this._container).find(".slider-overlay"   );
        if ( Othumbs.length>0 ) {
            // update overlays when thumb clicked
            Othumbs.click( function() {
                // set selected thumb
                $(this).toggleClass("selected");
                // update map layers
                var names = [];
                Othumbs.filter(".selected").each( function() {
                    names.push( $(this).data("name") );
                });
                util.addReferenceMap({
                    map     : map,
                    name    : names,
                    opacity : parseFloat(Oslider.val()),
                    clear   : true
                });
            });
            // create slider and change opacity when changes
            Oslider.slider({ from:0, to:1, step:0.05, value:this.options.overlayOpacity, onChange:function(val){
                // change opacity
                if (map.layers && map.layers.referenceLayerGroup) {
                    map.layers.referenceLayerGroup.eachLayer( function(layer) { layer.setOpacity(parseFloat(val)); });
                }
                // update slider label value
                Oslider.parent().find(".slider-value").html( (100*parseFloat(val)).toFixed(0)+"%" );
            }});
            // show specified on startup
            $(this.options.overlays).each( function(idx,o){
                if (o.visible===true) { Othumbs.eq(idx).click(); }
            });
        } else {
            // none - remove section
            Oslider.closest(".well").remove();
        }

        // clicking or entering control puts on top of other sibling controls
        $("#"+this.idBtn+",#"+this.idPnl).on( "click mouseenter", function(){
            $(this).closest(".leaflet-control")
                .css({"z-index":2})
                .siblings().css({"z-index":1});
        });

        // clicking map closes panel
        map.on("click", this.close, this);

        // done
        return this;
    },

    onRemove: function(map) {
        // cleanup
        map.off("click", this.close, this);
        delete map.controls[this._controlID];
    }
    
});

// constructor registration
L.control.basepicker = function(options) {
    return new L.Control.BasePicker(options);
};

// add new map constructor option to create control
L.Map.mergeOptions({
    basepickerControl: false
});

// add control when map created if map option set
L.Map.addInitHook( function() {
    if (this.options.basepickerControl) {
        this.addControl(
            L.control.basepicker(
                ( this.options.basepickerControl===true ? {} : this.options.basepickerControl ) // default options when true or specified options object
            )
        );
    }
});


//-----------------------------------------------------------
// L.Control.ButtonPanel
//   add button panel control
//   control is button that opens/closes panel with user-specified content
//   can be used for data layer picker, legend, ect
//
// see public methods below for those available
//
// IMPORTANT: requires util_ui
//-----------------------------------------------------------

// constructor
L.Control.ButtonPanel = L.Control.extend({
    
    //------------------------------
    // public
    //------------------------------

    // creation options
    options: {
        position    : "topright",  // position on map, "topleft", "topright", "bottomleft", "bottomright"
        label       : "Panel",     // button label
        buttonWidth : undefined,   // button width [px], omit for auto
        panelWidth  : 300,         // expanded panel width  [px], overflow scrolls, omit for auto
        panelHeight : undefined,   // expanded panel height [px], overflow scrolls, omit for auto
        contentHtml : "",          // panel content html string
        title       : ""           // button hover tooltip
    },
    
    // set content html
    setContentHtml: function(html) {
        $(this._container).find(".leaflet-control-buttonpanel-content").empty().html( html );
    },

    // open
    open: function(){
        util.togglePanel( "#"+this.idPnl, true );
    },

    // close
    close: function(){
        util.togglePanel( "#"+this.idPnl, false );
    },

    //------------------------------
    // private
    //------------------------------
    
    onAdd: function(map) {

        // create container
        this._container = L.DomUtil.create("div","leaflet-control-buttonpanel util-ui-unselectable");
        
        // disable map mouse events in container
        L.DomEvent.disableClickPropagation( this._container);
        L.DomEvent.disableScrollPropagation(this._container);

        // set unique button and panel id's
        var r = Math.floor(1000*Math.random());
        this.idBtn = "util_leaflet_buttonpanel_btn"+r;
        this.idPnl = "util_leaflet_buttonpanel_pnl"+r;

        // add button to container and append panel
        this.options.buttonWidth = ( this.options.buttonWidth===undefined ? "auto" : this.options.buttonWidth+"px" );
        this.options.panelWidth  = ( this.options.panelWidth ===undefined ? "auto" : this.options.panelWidth +"px" );
        this.options.panelHeight = ( this.options.panelHeight===undefined ? "auto" : this.options.panelHeight+"px" );
        $(this._container).html( '<button id="'+this.idBtn+'" type="button" class="btn btn-primary" title="'+this.options.title+'" style="width:'+this.options.buttonWidth+';">'+this.options.label+'</button>' ).append(
            '<div id="'+this.idPnl+'">'+
                '<div class="leaflet-control-buttonpanel-content" style="margin-bottom:5px; width:'+this.options.panelWidth+'; height:'+this.options.panelHeight+'; overflow:auto;">'+
                    this.options.contentHtml +
                '</div>'+
                '<button type="button" class="btn btn-default btn-xs" onclick="util.togglePanel(\'#'+this.idPnl+'\',false);" style="float:right;">Close</button>'+
            '</div>'
        );

        // add to map.controls and return container
        if (map.controls===undefined) { map.controls={}; }
        var controlN=0; var doneN=false;
        while(!doneN) {
            controlN++;  this._controlID="buttonpanel"+controlN;
            if(map.controls[this._controlID]===undefined){ doneN=true; }
        }
        map.controls[this._controlID] = this;
        return this._container;
    },
    
    addTo: function(map) {
        L.Control.prototype.addTo.call( this, map );

        // finalize when control container added to map and accessible:

        // create panel
        var position;
        switch(this.options.position.toLowerCase()) {
            case "bottomleft"  :           position=  "up-left" ; break;
            case "bottomright" :           position=  "up-right"; break;
            case "topleft"     :           position="down-left" ; break;
            case "topright"    : default : position="down-right"; break;
        }
        util.createPanel( "#"+this.idBtn, "#"+this.idPnl, "#"+this.idBtn, position);
        
        // clicking or entering control puts on top of other sibling controls
        $("#"+this.idBtn+",#"+this.idPnl).on( "click mouseenter", function(){
            $(this).closest(".leaflet-control")
                .css({"z-index":2})
                .siblings().css({"z-index":1});
        });

        // clicking map closes panel
        map.on("click", this.close, this);
        return this;
    },

    onRemove: function(map) {
        // cleanup
        map.off("click", this.close, this);
        delete map.controls[this._controlID];
    }
    
});

// constructor registration
L.control.buttonpanel = function(options) {
    return new L.Control.ButtonPanel(options);
};

// add new map constructor option to create control
L.Map.mergeOptions({
    buttonpanelControl: false
});

// add control when map created if map option set
L.Map.addInitHook( function() {
    if (this.options.buttonpanelControl) {
        this.addControl(
            L.control.buttonpanel(
                ( this.options.buttonpanelControl===true ? {} : this.options.buttonpanelControl ) // default options when true or specified options object
            )
        );
    }
});


//-----------------------------------------------------------
// L.Control.CenterPanel
//   add center panel control
//   control is semi-transparent div centered at the top or bottom of map
//   can be used for the map title, control container, etc
//
// see public methods below for those available
//
// IMPORTANT: requires util_ui
//-----------------------------------------------------------

// constructor
L.Control.CenterPanel = L.Control.extend({
    
    //------------------------------
    // public
    //------------------------------

    // creation options
    options: {
        topBottom   : "top",      // position on map, "top", "bottom"
        panelWidth  : undefined,  // panel width  [px], overflow scrolls, omit for auto
        panelHeight : undefined,  // panel height [px], overflow scrolls, omit for auto
        contentHtml : ""          // panel content html string
    },
    
    // set content html
    setContentHtml: function(html) {
        $(this._container).empty().html( html );
        util.centerMes();
    },

    // hide
    hide: function(){
        $(this._container).stop().fadeOut(200);
    },

    // show
    show: function(){
        $(this._container).stop().fadeIn(200);
    },

    // toggle
    toggle: function(){
        if ( $(this._container).is(":hidden") ) {
            this.show();
        } else {
            this.hide();
        }
    },

    //------------------------------
    // private
    //------------------------------
    
    onAdd: function(map) {
        // create container
        this._container = L.DomUtil.create("div","leaflet-control-centerpanel util-ui-unselectable util-ui-center-me-h");
        
        // disable map mouse events in container
        L.DomEvent.disableClickPropagation( this._container);
        L.DomEvent.disableScrollPropagation(this._container);

        // add content to container
        this.options.panelWidth  = ( this.options.panelWidth ===undefined ? "auto" : this.options.panelWidth +"px" );
        this.options.panelHeight = ( this.options.panelHeight===undefined ? "auto" : this.options.panelHeight+"px" );
        $(this._container).html(
            '<div class="leaflet-control-centerpanel-content" style="width:'+this.options.panelWidth+'; height:'+this.options.panelHeight+'; overflow:auto;">'+
                this.options.contentHtml +
            '</div>'
        );

        // add to map.controls and return container
        if (map.controls===undefined) { map.controls={}; }
        var controlN=0; var doneN=false;
        while(!doneN) {
            controlN++;  this._controlID="centerpanel"+controlN;
            if(map.controls[this._controlID]===undefined){ doneN=true; }
        }
        map.controls[this._controlID] = this;
        return this._container;
    },
    
    addTo: function(map) {
        L.Control.prototype.addTo.call( this, map );

        // finalize when control container added to map and accessible:

        // add custom custom centered top & bottom control containers if needed
        var controlContainer = $(map.getContainer()).find(".leaflet-control-container").eq(0);
        if ( controlContainer.find(".leaflet-center-top").length <= 0 ) {
            controlContainer.append(
                '<div class="leaflet-center-top leaflet-top"></div>'
            );
        }
        if ( controlContainer.find(".leaflet-center-bottom").length <= 0 ) {
            controlContainer.append(
                '<div class="leaflet-center-bottom leaflet-bottom"></div>'
            );
        }

        // move control to top or bottom container and center
        $(this._container)
            .removeClass("leaflet-top leaflet-bottom leaflet-left leaflet-right")
            .appendTo( controlContainer.find(".leaflet-center-"+this.options.topBottom) );
        util.centerMes();
        return this;
    },

    onRemove: function(map) {
        // cleanup
        delete map.controls[this._controlID];
    }
    
});

// constructor registration
L.control.centerpanel = function(options) {
    return new L.Control.CenterPanel(options);
};

// add new map constructor option to create control
L.Map.mergeOptions({
    centerpanelControl: false
});

// add control when map created if map option set
L.Map.addInitHook( function() {
    if (this.options.centerpanelControl) {
        this.addControl(
            L.control.centerpanel(
                ( this.options.centerpanelControl===true ? {} : this.options.centerpanelControl ) // default options when true or specified options object
            )
        );
    }
});

//===========================================================
// BASEMAP AND LAYERS
//===========================================================

// built-in basemap and reference layer urls, referenced by name
// urls are defined as arrays so multiple layers can be grouped together and handled as a single layerGroup (eg: base + labels)
// naming convention:
//   1 suffix = no   labels
//   2 suffix = with labels
util.layerUrls = {
    
    // openstreetmap
    // ...basemaps...
    openstreets: {
        urls: [
            "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"basemap"
    },
    
    // cartodb - see: https://github.com/CartoDB/cartodb/wiki/BaseMaps-available
    //   non-ssl: http://{s}.api.cartocdn.com/{basemap_id}/{z}/{x}/{y}.png
    //       ssl: https://cartocdn_{s}.global.ssl.fastly.net/{basemap_id}/{z}/{x}/{y}.png
    // ...basemaps...
    carto_base_light: {
        urls: [
            "https://cartocdn_{s}.global.ssl.fastly.net/base-light/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"basemap"
    },
    carto_base_eco1: {
        urls: [
            "https://cartocdn_{s}.global.ssl.fastly.net/base-eco/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"basemap"
    },
    carto_base_eco2: {
        urls: [
            "https://services.arcgisonline.com/ArcGIS/rest/services/Reference/World_Boundaries_and_Places_Alternate/MapServer/tile/{z}/{y}/{x}", // dark labels
            "https://cartocdn_{s}.global.ssl.fastly.net/base-eco/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"basemap"
    },
    carto_base_flatblue1: {
        urls: [
            "https://cartocdn_{s}.global.ssl.fastly.net/base-flatblue/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"basemap"
    },
    carto_base_flatblue2: {
        urls: [
            "https://services.arcgisonline.com/ArcGIS/rest/services/Reference/World_Boundaries_and_Places/MapServer/tile/{z}/{y}/{x}", // light labels
            "https://cartocdn_{s}.global.ssl.fastly.net/base-flatblue/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"basemap"
    },
    carto_base_antique1: {
        urls: [
            "https://cartocdn_{s}.global.ssl.fastly.net/base-antique/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"basemap"
    },
    carto_base_antique2: {
        urls: [
            "https://services.arcgisonline.com/ArcGIS/rest/services/Reference/World_Boundaries_and_Places_Alternate/MapServer/tile/{z}/{y}/{x}", // dark labels
            "https://cartocdn_{s}.global.ssl.fastly.net/base-antique/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"basemap"
    },
    carto_base_dark: {
        urls: [
            "https://cartocdn_{s}.global.ssl.fastly.net/base-dark/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"basemap"
    },
    carto_base_midnight: {
        urls: [
            "https://cartocdn_{s}.global.ssl.fastly.net/base-midnight/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"basemap"
    },
    
    // cartodb - see: https://carto.com/location-data-services/basemaps/
    //   non-ssl: http://{s}.basemaps.cartocdn.com/{basemap_id}/{z}/{x}/{y}.png
    //       ssl: https://cartodb-basemaps-{s}.global.ssl.fastly.net/{basemap_id}/{z}/{x}/{y}.png
    // ...basemaps...
    carto_positron1: {
        urls: [
            "https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_nolabels/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"basemap"
    },
    carto_positron2: {
        urls: [
            "https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"basemap"
    },
    carto_darkmatter1: {
        urls: [
            "https://cartodb-basemaps-{s}.global.ssl.fastly.net/dark_nolabels/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"basemap"
    },
    carto_darkmatter2: {
        urls: [
            "https://cartodb-basemaps-{s}.global.ssl.fastly.net/dark_all/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"basemap"
    },
    // ...reference...
    carto_dark_labels: {
        urls: [
            "https://cartodb-basemaps-{s}.global.ssl.fastly.net/dark_only_labels/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"reference"
    },
    carto_light_labels: {
        urls: [
            "https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_only_labels/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"reference"
    },
    
    // esri
    // ...basemaps...
    esri_topo: {
        urls: [
            "https://services.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}"
        ],
        minZoom:0, maxZoom:18, type:"basemap"
    },
    esri_imagery1: {
        urls: [
            "https://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}"
        ],
        minZoom:0, maxZoom:18, type:"basemap"
    },
    esri_imagery2: {
        urls: [
            "https://services.arcgisonline.com/ArcGIS/rest/services/Reference/World_Boundaries_and_Places/MapServer/tile/{z}/{y}/{x}", // light labels
            "https://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}"
        ],
        minZoom:0, maxZoom:18, type:"basemap"
    },
    esri_streets: {
        urls: [
            "https://services.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}" // has labels
        ],
        minZoom:0, maxZoom:18, type:"basemap"
    },
    esri_natgeo: {
        urls: [
            "https://services.arcgisonline.com/ArcGIS/rest/services/NatGeo_World_Map/MapServer/tile/{z}/{y}/{x}" // has labels
        ],
        minZoom:0, maxZoom:16, type:"basemap"
    },
    esri_usatopo: {
        urls: [
            "https://services.arcgisonline.com/ArcGIS/rest/services/USA_Topo_Maps/MapServer/tile/{z}/{y}/{x}" // has labels (note: uses physical for far out zoom levels)
        ],
        minZoom:0, maxZoom:15, type:"basemap"
    },
    esri_ocean: {
        urls: [
            "https://services.arcgisonline.com/ArcGIS/rest/services/Ocean_Basemap/MapServer/tile/{z}/{y}/{x}" // has labels
        ],
        minZoom:0, maxZoom:13, type:"basemap"
    },
    esri_relief1: {
        urls: [
            "https://services.arcgisonline.com/ArcGIS/rest/services/World_Shaded_Relief/MapServer/tile/{z}/{y}/{x}"
        ],
        minZoom:0, maxZoom:13, type:"basemap"
    },
    esri_relief2: {
        urls: [
            "https://services.arcgisonline.com/ArcGIS/rest/services/Reference/World_Boundaries_and_Places_Alternate/MapServer/tile/{z}/{y}/{x}", // dark labels
            "https://services.arcgisonline.com/ArcGIS/rest/services/World_Shaded_Relief/MapServer/tile/{z}/{y}/{x}"
        ],
        minZoom:0, maxZoom:13, type:"basemap"
    },
    esri_terrain1: {
        urls: [
            "https://services.arcgisonline.com/ArcGIS/rest/services/World_Terrain_Base/MapServer/tile/{z}/{y}/{x}"
        ],
        minZoom:0, maxZoom:13, type:"basemap"
    },
    esri_terrain2: {
        urls: [
            "https://services.arcgisonline.com/ArcGIS/rest/services/Reference/World_Boundaries_and_Places_Alternate/MapServer/tile/{z}/{y}/{x}", // dark labels
            "https://services.arcgisonline.com/ArcGIS/rest/services/World_Terrain_Base/MapServer/tile/{z}/{y}/{x}"
        ],
        minZoom:0, maxZoom:13, type:"basemap"
    },
    esri_physical1: {
        urls: [
            "https://services.arcgisonline.com/ArcGIS/rest/services/World_Physical_Map/MapServer/tile/{z}/{y}/{x}"
        ],
        minZoom:0, maxZoom:8, type:"basemap"
    },
    esri_physical2: {
        urls: [
            "https://services.arcgisonline.com/ArcGIS/rest/services/Reference/World_Boundaries_and_Places_Alternate/MapServer/tile/{z}/{y}/{x}", // dark labels
            "https://services.arcgisonline.com/ArcGIS/rest/services/World_Physical_Map/MapServer/tile/{z}/{y}/{x}"
        ],
        minZoom:0, maxZoom:8, type:"basemap"
    },
    esri_light_gray1: {
        urls: [
            "https://services.arcgisonline.com/arcgis/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}"
        ],
        minZoom:0, maxZoom:16, type:"basemap"
    },
    esri_light_gray2: {
        urls: [
            "https://services.arcgisonline.com/arcgis/rest/services/Canvas/World_Light_Gray_Reference/MapServer/tile/{z}/{y}/{x}", // labels
            "https://services.arcgisonline.com/arcgis/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}"
        ],
        minZoom:0, maxZoom:16, type:"basemap"
    },
    esri_dark_gray1: {
        urls: [
            "https://services.arcgisonline.com/arcgis/rest/services/Canvas/World_Dark_Gray_Base/MapServer/tile/{z}/{y}/{x}"
        ],
        minZoom:0, maxZoom:16, type:"basemap"
    },
    esri_dark_gray2: {
        urls: [
            "https://services.arcgisonline.com/arcgis/rest/services/Canvas/World_Dark_Gray_Reference/MapServer/tile/{z}/{y}/{x}", // labels
            "https://services.arcgisonline.com/arcgis/rest/services/Canvas/World_Dark_Gray_Base/MapServer/tile/{z}/{y}/{x}"
        ],
        minZoom:0, maxZoom:16, type:"basemap"
    },
    esri_hillshade1: {
        urls: [
            "https://services.arcgisonline.com/arcgis/rest/services/Elevation/World_Hillshade/MapServer/tile/{z}/{y}/{x}"
        ],
        minZoom:0, maxZoom:15, type:"basemap"
    },
    esri_hillshade2: {
        urls: [
            "https://services.arcgisonline.com/ArcGIS/rest/services/Reference/World_Boundaries_and_Places_Alternate/MapServer/tile/{z}/{y}/{x}", // dark labels
            "https://services.arcgisonline.com/arcgis/rest/services/Elevation/World_Hillshade/MapServer/tile/{z}/{y}/{x}"
        ],
        minZoom:0, maxZoom:15, type:"basemap"
    },
    // ...reference...
    esri_cities_borders_light: {
        urls: [
            "https://services.arcgisonline.com/ArcGIS/rest/services/Reference/World_Boundaries_and_Places/MapServer/tile/{z}/{y}/{x}"
        ],
        minZoom:1, maxZoom:18, type:"reference"
    },
    esri_cities_borders_dark: {
        urls: [
            "https://services.arcgisonline.com/ArcGIS/rest/services/Reference/World_Boundaries_and_Places_Alternate/MapServer/tile/{z}/{y}/{x}"
        ],
        minZoom:1, maxZoom:12, type:"reference"
    },
    esri_transportation: {
        urls: [
            "https://services.arcgisonline.com/ArcGIS/rest/services/Reference/World_Transportation/MapServer/tile/{z}/{y}/{x}"
        ],
        minZoom:3, maxZoom:18, type:"reference"
    },
    esri_reference: {
        urls: [
            "https://services.arcgisonline.com/ArcGIS/rest/services/Reference/World_Reference_Overlay/MapServer/tile/{z}/{y}/{x}"
        ],
        minZoom:0, maxZoom:13, type:"reference"
    },
    
    // usgs national map
    // ...basemaps...
    nationalmap_imagery1: {
        urls: [
            "https://basemap.nationalmap.gov/arcgis/rest/services/USGSImageryOnly/MapServer/tile/{z}/{y}/{x}"
        ],
        minZoom:0, maxZoom:15, type:"basemap"
    },
    nationalmap_imagery2: {
        urls: [
            "https://services.arcgisonline.com/ArcGIS/rest/services/Reference/World_Boundaries_and_Places/MapServer/tile/{z}/{y}/{x}", // light labels
            "https://basemap.nationalmap.gov/arcgis/rest/services/USGSImageryOnly/MapServer/tile/{z}/{y}/{x}"
        ],
        minZoom:0, maxZoom:15, type:"basemap"
    },
    nationalmap_imagery_topo: {
        urls: [
            "https://basemap.nationalmap.gov/arcgis/rest/services/USGSImageryTopo/MapServer/tile/{z}/{y}/{x}"
        ],
        minZoom:0, maxZoom:15, type:"basemap"
    },
    nationalmap_relief1: {
        urls: [
            "https://basemap.nationalmap.gov/arcgis/rest/services/USGSShadedReliefOnly/MapServer/tile/{z}/{y}/{x}"
        ],
        minZoom:0, maxZoom:15, type:"basemap"
    },
    nationalmap_relief2: {
        urls: [
            "https://services.arcgisonline.com/ArcGIS/rest/services/Reference/World_Boundaries_and_Places_Alternate/MapServer/tile/{z}/{y}/{x}", // dark labels
            "https://basemap.nationalmap.gov/arcgis/rest/services/USGSShadedReliefOnly/MapServer/tile/{z}/{y}/{x}"
        ],
        minZoom:0, maxZoom:15, type:"basemap"
    },
    nationalmap_topo: {
        urls: [
            "https://basemap.nationalmap.gov/ArcGIS/rest/services/USGSTopo/MapServer/tile/{z}/{y}/{x}"
        ],
        minZoom:0, maxZoom:15, type:"basemap"
    },
    // ...reference...
    nationalmap_nhd: {
        urls: [
            "https://basemap.nationalmap.gov/arcgis/rest/services/USGSHydroCached/MapServer/tile/{z}/{y}/{x}"
        ],
        minZoom:0, maxZoom:16, type:"reference"
    },
    
    // nwismapper
    // ...basemaps...
    nwismapper_hucs: {
        urls: [
            "https://nwismapper.s3.amazonaws.com/hucs/{z}/{y}/{x}.png",
            "https://services.arcgisonline.com/ArcGIS/rest/services/Reference/World_Boundaries_and_Places_Alternate/MapServer/tile/{z}/{y}/{x}", // dark labels
            "https://cartocdn_{s}.global.ssl.fastly.net/base-eco/{z}/{x}/{y}.png" // base
        ],
        minZoom:0, maxZoom:11, type:"basemap"
    },
    nwismapper_aquifer: {
        urls: [
            "https://nwismapper.s3.amazonaws.com/pr_aq/{z}/{y}/{x}.png",
            "https://cartocdn_{s}.global.ssl.fastly.net/base-light/{z}/{x}/{y}.png" // base
        ],
        minZoom:4, maxZoom:11, type:"basemap"
    },
    // ...reference...
    nwismapper_hucs_ref: {
        urls: [
            "https://nwismapper.s3.amazonaws.com/hucs/{z}/{y}/{x}.png"
        ],
        minZoom:0, maxZoom:11, type:"reference"
    },
    nwismapper_aquifer_ref: {
        urls: [
            "https://nwismapper.s3.amazonaws.com/pr_aq/{z}/{y}/{x}.png"
        ],
        minZoom:4, maxZoom:11, type:"reference"
    },
    
    // stamen
    // ...basemaps...
    stamen_terrain1: {
        urls: [
            "https://stamen-tiles-{s}.a.ssl.fastly.net/terrain-lines/{z}/{x}/{y}.png",
            "https://stamen-tiles-{s}.a.ssl.fastly.net/terrain-background/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:12, type:"basemap"
    },
    stamen_terrain2: {
        urls: [
            "https://stamen-tiles-{s}.a.ssl.fastly.net/terrain/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:12, type:"basemap"
    },
    stamen_toner_lite: {
        urls: [
            "https://stamen-tiles-{s}.a.ssl.fastly.net/toner-lite/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"basemap"
    },
    stamen_toner1: {
        urls: [
            "https://stamen-tiles-{s}.a.ssl.fastly.net/toner-background/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"basemap"
    },
    stamen_toner2: {
        urls: [
            "https://stamen-tiles-{s}.a.ssl.fastly.net/toner/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"basemap"
    },
    stamen_watercolor1: {
        urls: [
            "https://stamen-tiles-{s}.a.ssl.fastly.net/terrain-lines/{z}/{x}/{y}.png",
            "https://stamen-tiles-{s}.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"basemap"
    },
    stamen_watercolor2: {
        urls: [
            "https://stamen-tiles-{s}.a.ssl.fastly.net/terrain-labels/{z}/{x}/{y}.png",
            "https://stamen-tiles-{s}.a.ssl.fastly.net/terrain-lines/{z}/{x}/{y}.png",
            "https://stamen-tiles-{s}.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"basemap"
    },
    
    // mesonet
    // see: https://mesonet.agron.iastate.edu/ogc/
    // ...reference...
    mesonet_nexrad: { // nexrad base reflectivity (current)
        urls: [
            "https://mesonet.agron.iastate.edu/cache/tile.py/1.0.0/nexrad-n0q-900913/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"reference"
    },
    mesonet_hybrid: { // MRMS hybrid-scan reflectivity composite (similar to base reflectivity but different color scale)
        urls: [
            "https://mesonet.agron.iastate.edu/cache/tile.py/1.0.0/q2-hsr-900913/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"reference"
    },
    mesonet_eet: { // nexrad echo tops EET (current)
        urls: [
            "https://mesonet.agron.iastate.edu/cache/tile.py/1.0.0/nexrad-eet-900913/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"reference"
    },
    mesonet_precip1hr: { // Q2 1 hour precipitation
        urls: [
            "https://mesonet.agron.iastate.edu/cache/tile.py/1.0.0/q2-n1p-900913/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"reference"
    },
    mesonet_precip24hr: { // Q2 24 hour precipitation
        urls: [
            "https://mesonet.agron.iastate.edu/cache/tile.py/1.0.0/q2-p24h-900913/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"reference"
    },
    mesonet_precip48hr: { // Q2 48 hour precipitation
        urls: [
            "https://mesonet.agron.iastate.edu/cache/tile.py/1.0.0/q2-p48h-900913/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"reference"
    },
    mesonet_precip72hr: { // Q2 72 hour precipitation
        urls: [
            "https://mesonet.agron.iastate.edu/cache/tile.py/1.0.0/q2-p72h-900913/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"reference"
    },
    
    // realearth
    // many many services  - see: https://realearth.ssec.wisc.edu/products/
    // for legend try  https://realearth.ssec.wisc.edu/api/legend?products=[ID]  where [ID] is in url as .../tiles/[ID]/...
    realearth_globalvis: { // Global Imagery: visible - full
        urls: [
            "https://realearth.ssec.wisc.edu/tiles/globalvis/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"reference"
    },
    realearth_globalir: { // global infrared, grayscale
        urls: [
            "https://realearth.ssec.wisc.edu/tiles/globalir/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"reference"
    },
    realearth_globalir_funk: { // global infrared, color
        urls: [
            "https://realearth.ssec.wisc.edu/tiles/globalir-funk/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"reference"
    },
    realearth_globalwv: { // global water vapor, grayscale
        urls: [
            "https://realearth.ssec.wisc.edu/tiles/globalwv/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"reference"
    },
    realearth_globalwv_grad: { // global water vapor, color
        urls: [
            "https://realearth.ssec.wisc.edu/tiles/globalwv-grad/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"reference"
    },
    realearth_sfcTemp: { // conus temperature raster - legend see: http://realearth.ssec.wisc.edu/?products=sfcTemp
        urls: [
            "https://realearth.ssec.wisc.edu/tiles/sfcTemp/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"reference"
    },
    realearth_SFCCON_T: { // conus temperature contours
        urls: [
            "https://realearth.ssec.wisc.edu/tiles/SFCCON-T/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"reference"
    },
    realearth_SFCCON_PMSL: { // conus pressure contours
        urls: [
            "https://realearth.ssec.wisc.edu/tiles/SFCCON-PMSL/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"reference"
    },
    realearth_NESDIS_SST: { // global sea surface temperature - legend see: http://realearth.ssec.wisc.edu/?products=NESDIS-SST
        urls: [
            "https://realearth.ssec.wisc.edu/tiles/NESDIS-SST/{z}/{x}/{y}.png"
        ],
        minZoom:0, maxZoom:18, type:"reference"
    },
    
    // USGS TX-WSC
    // ...reference...
    txwsc_usa_mask: {
        urls: [
            "https://txgeo.usgs.gov/arcgis/rest/services/Mapping/Mask/MapServer/tile/{z}/{y}/{x}"
        ],
        minZoom:4, maxZoom:11, type:"reference"
    },
    txwsc_hydro_light: {
        urls: [
            "https://txgeo.usgs.gov/arcgis/rest/services/Mapping/HydroBaseMapForTerrain/MapServer/tile/{z}/{y}/{x}"
        ],
        minZoom:4, maxZoom:11, type:"reference"
    },
    txwsc_hydro_dark: {
        urls: [
            "https://txgeo.usgs.gov/arcgis/rest/services/Mapping/HydroBaseMapForImagery/MapServer/tile/{z}/{y}/{x}"
        ],
        minZoom:4, maxZoom:11, type:"reference"
    },
    txwsc_ocean: {
        urls: [
            "https://txgeo.usgs.gov/arcgis/rest/services/Mapping/Ocean/MapServer/tile/{z}/{y}/{x}"
        ],
        minZoom:4, maxZoom:11, type:"reference"
    }
    
    
}; // layerUrls

//-----------------------------------------------------------
// changeBaseMap
//   add or update map basemap layer
//   layerGroup object added to map: map.layers.basemapLayerGroup
//   only 1 basemapLayerGroup can be on the map at once (adding new removes existing)
//
//   opts - see below
//
//   can also just input basemap "name" (opacity will be default)
//   if no input provided, lists available basemap names
//
// EXAMPLES:
// changeBaseMap();                                       // list available basemap names to console
// changeBaseMap("carto_base_light");                     // add this basemap with 100% opacity (previously added removed)
// changeBaseMap({ name:"carto_base_light", opacity:1 }); // same as above
// changeBaseMap({ name:"esri_natgeo", opacity:0.5 });    // add this basemap with 50% opacity (previously added removed)
// changeBaseMap("INVALID_NAME");                         // invalid name will default to "openstreets"
// changeBaseMap({ opacity:0.5 });                        // set last added basemap opacity 50%
// changeBaseMap("none");                                 // remove last added basemap
util.changeBaseMap = function ( opts ) {
    var funcName = "util [changeBaseMap]: ";
    
    // list available basemap names if no input
    if (opts === undefined) {
        console.log(funcName + "available basemap names:");
        $.each( util.layerUrls, function( name, urls ) {
            console.log( name );
        });
        return 0;
    }
    
    // if string input, set as basemap name with default opacity
    if (typeof opts === "string") {
        var name = opts;
        opts = {};
        opts.name = name;
    }
    
    // set opts defaults
    if (opts === undefined) { opts = {}; }
    if (opts.map     === undefined) { opts.map     = util.map; } // leaflet map
    if (opts.name    === undefined) { opts.name    = "same";   } // basemap name defined in layerUrls obj - omit or set "same" to use existing, set "none" to remove
    if (opts.opacity === undefined) { opts.opacity = 1;        } // opacity, 0 to 1
    util.map = opts.map;
    
    // create basemapLayerGroup if needed
    if (opts.map.layers === undefined) { opts.map.layers = {}; }
    if (opts.map.layers.basemapLayerGroup === undefined) {
        opts.map.layers.basemapLayerGroup = L.layerGroup().addTo( opts.map );
    }
    
    // handle "same" - apply opts to layerGroup
    if (opts.name.toLowerCase() === "same") { 
        opts.map.layers.basemapLayerGroup.eachLayer( function (layer) {
            layer.setOpacity( opts.opacity );
            layer.bringToBack();
        });
        return 0; // done
    }
    
    // clear layerGroup
    // console.log(funcName + opts.name);
    opts.map.layers.basemapLayerGroup.clearLayers();
    
    // if "none", we are done
    if (opts.name.toLowerCase() === "none") { return 0; }
    
    // get layer info for input name
    var lyrInfo = util.layerUrls[ opts.name.toLowerCase() ];
    if ( lyrInfo === undefined ) {
        // does not exist - use default
        console.warn(funcName + "invalid basemap name '" + opts.name + "' - 'openstreets' used");
        lyrInfo = util.layerUrls[ "openstreets" ];
    }
    
    // add new
    $.each( lyrInfo.urls, function( idx, url ) {
        opts.map.layers.basemapLayerGroup.addLayer(
            L.tileLayer( url, {
                opacity       : opts.opacity,    // input
                minNativeZoom : lyrInfo.minZoom, // extend to all zoom levels using defined min-max available zoom
                maxNativeZoom : lyrInfo.maxZoom
            })
        );
    });
    
    // bring to bottom of map
    opts.map.layers.basemapLayerGroup.eachLayer( function(layer) {
        layer.bringToBack();
    });
    
}; // changeBaseMap


//-----------------------------------------------------------
// addReferenceMap
//   add reference layer to map
//   layerGroup added to map: map.layers.referenceLayerGroup
//   new reference layers are appended to the referenceLayerGroup
//   see opts for clearing previously added reference layers
//
//   opts - see below
//
//   can also just input reference map "name" (opacity will be default)
//   if no input provided, lists available reference map names
//
// EXAMPLES:
// util.map = map; // specify map
// util.addReferenceMap("esri_transportation");                                          // add this reference layer with 100% opacity
// util.addReferenceMap({ name:"esri_transportation", opacity:1 });                      // same as above
// util.addReferenceMap({ name:["esri_transportation","esri_reference"], opacity:0.5 }); // add these 2 reference layers with 50% opacity, keep any previously added
// util.addReferenceMap({ name:"esri_transportation", clear:true });                     // add this reference layer with 100% opacity, clear any previously added
// util.addReferenceMap({ clear:true });                                                 // add no reference layer but clear any previously added
util.addReferenceMap = function ( opts ) {
    var funcName = "util [addReferenceMap]: ";

    var validNames = [];
    $.each( util.layerUrls, function(name,props) {
        if (props.type==="reference") {
            validNames.push( name );
        }
    });
    if (opts === undefined) { // list available if no input
        console.log(funcName + "available reference maps:");
        $.each( validNames, function( idx, validName ){
            console.log(validName);
        });
        return 0;
    }
    
    // if string input, set as basemap name
    if (typeof opts === "string") {
        var name = opts;
        opts = {};
        opts.name = name;
    }
    
    // set opts defaults
    if (opts === undefined) { opts = {}; }
    if (opts.map     === undefined) { opts.map     = util.map; } // leaflet map
    if (opts.name    === undefined) { opts.name    = "";       } // one of above validNames (string) or array of validNames - if omitted no reference layer added
    if (opts.opacity === undefined) { opts.opacity = 1;        } // opacity of added layer(s), 0 to 1
    if (opts.clear   === undefined) { opts.clear   = false;    } // whether to clear all reference layers previously added with this function
    util.map = opts.map;
    
    // create referenceLayerGroup if needed
    if (opts.map.layers === undefined) { opts.map.layers = {}; }
    if (opts.map.layers.referenceLayerGroup === undefined) {
        opts.map.layers.referenceLayerGroup = L.layerGroup();
        opts.map.layers.referenceLayerGroup.addTo( opts.map );
    }
    
    // clear if specified
    console.log(funcName + opts.name);
    if (opts.clear === true) {
        opts.map.layers.referenceLayerGroup.clearLayers();
    }
    
    // if no "name", we are done
    if (opts.name === "") { return 0; }
    
    // convert name to array if needed
    if (typeof opts.name === "string") {
        opts.name = [ opts.name ];
    }
    
    // make sure name(s) are valid
    var badName = "";
    $.each( opts.name, function( idx, name ){
        if ( $.inArray(name,validNames) <= -1) {
            badName = name;
            return;
        }
    });
    if ( badName !== "") {
        console.warn(funcName + "invalid reference map name: '" + badName + "'");
        console.warn(funcName + "valid reference map names are:");
        $.each( validNames, function( idx, validName ){
            console.warn(validName);
        });
        return 1;
    }
    
    // add layer(s) to layerGroup
    $.each( opts.name, function( idx, name ) {
        opts.map.layers.referenceLayerGroup.addLayer(
            L.tileLayer(
                util.layerUrls[name].urls[0], // all reference layers just have 1 url
                {
                    opacity       : opts.opacity,                 // input
                    minNativeZoom : util.layerUrls[name].minZoom, // extend to all zoom levels using defined min-max available zoom
                    maxNativeZoom : util.layerUrls[name].maxZoom
                }
            )
        );
    });
    
    // bring to top of map
    opts.map.layers.referenceLayerGroup.eachLayer( function(layer) {
        layer.bringToFront();
    });
    
}; // addReferenceMap


//-----------------------------------------------------------
// L.nexrad
//    create L.layerGroup containing weather radar
//    supports animated looping
//    see default options below for all supported options
//    returns the layerGroup
//
// USAGE:
//    L.nexrad( <object> options? )
//
// EXAMPLE:
//    L.nexrad().addTo(map);
//
// EXAMPLE:
//    var layerGroup = L.nexrad({
//        opacity  : 0.7,
//        loop     : true,
//        loopOn   : true,
//        loopMs   : 200,
//        onUpdate : function(lyrgrp){
//            console.warn("currently showing radar @ "+lyrgrp.getDate());
//        }
//    });
//    map.addLayer(layerGroup);
//
// the layerGroup returned has standard properties and methods of a L.layerGroup
//
// these additional extended methods are also available:
// ...static and loop...
//    layerGroup.setOpacity(opacity) [returns layerGroup     ] set opacity
//    layerGroup.getDate()           [returns datetime object] get date of the displayed layer
//    layerGroup.getAgeMinutes()     [returns integer        ] get age of the displayed layer, rounded to the nearest minute
// ...loop only...
//    layerGroup.loopOn(boolean)     [returns layerGroup]      turn looping off (false, pause), on (true, resume), or toggle (no input)
//    layerGroup.setLoopMs(millisec) [returns layerGroup]      update the frame rate [milliseconds], can be executed when the loop is running or stopped
//
// these additional extended properties are also available:
//    layerGroup.isLooping           [boolean]                 whether animation loop is running (always false when looping disabled)
//
// DEV NOTE: this is a lot more fancy but requires supporting library: http://apps.socib.es/Leaflet.TimeDimension/examples/example14.html
L.nexrad = function( options ) {
    
    //------------------------------
    // default options
    //------------------------------
    var defaultOptions = {
        // opacity [number between 0 and 1 inclusive]
        //   layer opacity on creation
        opacity: 0.5,

        // className [string]
        //   custom class(es) to add to layers
        //   separate multiple classes with spaces
        //   omit or set to empty string to apply no custom classes
        className: "",

        // loop [boolean]
        //   whether to enable animated loop
        //   when false a static layer of current radar is returned and all other looping options are ignored
        //   the static layer is updated every 5 minutes from source but cannot be made loop-able after creation
        loop: false,

        // loopOn [boolean]
        //   when looping enabled, whether to create with looping animation on (true)
        //   set false to create with loop stopped and use the loopOn(true) method later to start
        //   has no effect when looping disabled (loop false)
        loopOn: false,

        // loopMs [number greater than 0]
        //   when looping enabled, the animation loop frame rate, in milliseconds
        //   has no effect when looping disabled (loop false)
        loopMs: 400,
        
        // loopType [string]
        //   loop layer type, either:
        //     "n0q" - nexrad base reflectivity (normal weather radar, default)
        //     "eet" - nexrad echo tops EET (maximum height of precipitation echoes, advanced usage)
        //   this option is only available when looping is enabled
        loopType: "n0q",
        
        // onUpdate [function]
        //   function to execute when displayed layer is updated
        //   when looping is disabled (static), this occurs when layer is added to map and for each 5 minute update
        //   when looping is enabled, also occurs with each new animation frame
        //   can be useful to update any date-time annotation in the webpage using the getDate() or getAgeMinutes() methods
        //   default is to do nothing
        onUpdate: function(lyrgrp){},
    };
    
    //.............................
    // create layer group and set options
    var layerGroup = L.layerGroup();
    layerGroup._options = L.Util.extend( defaultOptions, options );

    // init some props
    layerGroup._iActive  = 0;     // active frame index
    layerGroup.isLooping = false; // whether looping is on (public)
    
    //.............................
    // public methods

    // change opacity
    layerGroup.setOpacity = function(opacity){
        layerGroup._options.opacity = opacity;
        layerGroup.getLayers()[layerGroup._iActive].setOpacity( opacity );
        return layerGroup;
    };

    // get currently displayed date-time [date object]
    layerGroup.getDate = function(){
        return layerGroup.getLayers()[ layerGroup._iActive ]._date;
    };

    // get age [minutes] of currently displayed layer
    layerGroup.getAgeMinutes = function(){
        return Math.round(  ( new Date() - layerGroup.getDate() ) / (60*1000)  );
    };

    //.............................
    // add layers
    if (layerGroup._options.loop===false) {

        // static - use higher resolution nowcoast
        // for more info see: https://nowcoast.noaa.gov/arcgis/rest/services/nowcoast/radar_meteo_imagery_nexrad_time/MapServer
        layerGroup.addLayer(
            L.tileLayer.wms( "https://nowcoast.noaa.gov/arcgis/services/nowcoast/radar_meteo_imagery_nexrad_time/MapServer/WMSServer", {
                //...map server options...
                format      : "image/png",
                transparent : true,
                layers      : 1,
                time        : "", // omit or "" for current, date string like "2017-07-31T13:24:00.000Z" for specific time (UTC), availability appears to be every 4 minutes: xx:00, xx:04, ..., xx:56, xx:00
                //...user options...
                opacity     : layerGroup._options.opacity,
                className   : layerGroup._options.className
            })
        );
        layerGroup.getLayers()[0]._date = new Date();

    } else {

        // loop - use better performing mesonet
        // past 50 minutes in 5 minute increments available:
        //   "nexrad-n0q-900913"       current
        //   "nexrad-n0q-900913-m50m"  50 minutes old
        //   "nexrad-n0q-900913-m45m"  45 minutes old
        //   "nexrad-n0q-900913-m40m"  ...etc...
        //   "nexrad-n0q-900913-m35m"
        //   "nexrad-n0q-900913-m30m"
        //   "nexrad-n0q-900913-m25m"
        //   "nexrad-n0q-900913-m20m"
        //   "nexrad-n0q-900913-m15m"
        //   "nexrad-n0q-900913-m10m"
        //   "nexrad-n0q-900913-m05m"
        // for more info see: https://mesonet.agron.iastate.edu/ogc/
        $.each( [ 0, 50, 40, 30, 20, 10 ], function(idx,MM){ // use 10 minute increments
            // add layer
            layerGroup.addLayer(
                L.tileLayer( "https://mesonet{s}.agron.iastate.edu/cache/tile.py/1.0.0/nexrad-"+layerGroup._options.loopType.toLowerCase()+"-900913"+(MM===0 ? "" : "-m"+MM+"m")+"/{z}/{x}/{y}.png", {
                    subdomains : "123", // using {s} subdomains greatly improves performance
                    opacity    : (idx===0 ? layerGroup._options.opacity : 0),
                    className  : layerGroup._options.className
                })
                    .on("loading", function(){ this._loaded = false; }) // do not go to this loop frame while tiles are still loading
                    .on("load",    function(){ this._loaded = true;  }) // tiles loaded - ok to go to this loop frame
            );
            // set layer date-time
            layerGroup.getLayers()[idx]._date = new Date( new Date() - MM*60*1000 );
        });

        //.............................
        // public methods for loop

        // start-stop-toggle loop
        layerGroup.loopOn = function(start){
            if (start===undefined) { start = !layerGroup.isLooping } // toggle
            if (start && !layerGroup.isLooping) { // start and not already started
                // start interval
                layerGroup._loopInterval = setInterval( function(){
                    layerGroup._iActive = (layerGroup._iActive+1) % layerGroup.getLayers().length;
                    if (layerGroup.getLayers()[layerGroup._iActive]._loaded ) {
                        // next frame loaded - set visible and all others hidden
                        for (var i=0; i<layerGroup.getLayers().length; i++){
                            layerGroup.getLayers()[i].setOpacity( i===layerGroup._iActive ? layerGroup._options.opacity : 0 );
                        }
                        // trigger update
                        if (typeof layerGroup._options.onUpdate==="function"){ layerGroup._options.onUpdate(layerGroup); }
                    }
                }, layerGroup._options.loopMs );
                layerGroup.isLooping = true;
            } else if (!start && layerGroup.isLooping) { // stop and not already stopped
                // stop interval
                clearInterval( layerGroup._loopInterval );
                // set 1st frame (most recent) active
                layerGroup._iActive = 0;
                for (var i=0; i<layerGroup.getLayers().length; i++){
                    layerGroup.getLayers()[i].setOpacity( i===layerGroup._iActive ? layerGroup._options.opacity : 0 );
                }
                layerGroup.isLooping = false;
            }
            return layerGroup;
        };

        // change loop frame rate
        layerGroup.setLoopMs = function(loopMs){
            // set option
            layerGroup._options.loopMs = loopMs;
            // stop and restart if looping
            if (layerGroup.isLooping) { layerGroup.loopOn(false).loopOn(true); }
            return layerGroup;
        };
    }

    // on add to map
    layerGroup.on( "add", function(){
        
        // start-stop loop
        layerGroup.loopOn( layerGroup._options.loopOn );
        
        // redraw all layers every 5 minutes to update from source
        layerGroup._redrawInterval = setInterval( function(){
            // redraw
            layerGroup.eachLayer( function(lyr){ lyr.redraw(); });
            // trigger update
            if (typeof layerGroup._options.onUpdate==="function"){ layerGroup._options.onUpdate(layerGroup); }
            // update layer _date
            if (layerGroup.getLayers().length>1) {
                // loop frames
                $.each( [ 0, 50, 40, 30, 20, 10 ], function(idx,MM){
                    layerGroup.getLayers()[idx]._date = new Date( new Date() - MM*60*1000 );
                });
            } else {
                // single static
                layerGroup.getLayers()[0]._date = new Date();
            }
        }, 5*60*1000 );
        
        // trigger update
        if (typeof layerGroup._options.onUpdate==="function"){ layerGroup._options.onUpdate(layerGroup); }
    });
    
    // on remove from map
    layerGroup.on( "remove", function(){
        clearInterval( layerGroup._redrawInterval );
        layerGroup.loopOn(false);
    });
    
    // done
    return layerGroup;
};


//-----------------------------------------------------------
// L.nwsWatches
//    create L.geoJSON layer containing National Weather Service watches, warnings, and advisories.
//    returns the layer.
//
// USAGE:
//    L.nwsWatches( <object> options? )
//
// EXAMPLE:
//    L.nwsWatches({ opacity:1 }).addTo(map);
//
// the layer returned has standard properties and methods of a L.geoJSON layer.
// these additional extended methods are also available:
//
//    layer.setOpacity(opacity)         [returns layer] set layer opacity
//    layer.refresh()                   [returns layer] refresh layer with new service call
//
L.nwsWatches = function( options ) {
    
    //------------------------------
    // default options
    //------------------------------
    var defaultOptions = {
        
        //...............
        // data
        //...............

        // latLngBounds [L.latLngBounds]
        //   only get features that intersect this bounding box (contained within and crosses box boundary)
        //   omit or set undefined to get all available features
        //   can greatly speed up data request when specified
        latLngBounds: undefined,

        // autoRefresh [boolean]
        //   whether to auto-refresh the layer every 5 minutes to update from source data
        autoRefresh: true,

        //...............
        // appearance
        //...............

        // opacity [number between 0 and 1 inclusive]
        //   outline and fill opacity
        opacity: 0.5,
        
        // interactive [boolean]
        //   whether to enable mouse-click popup
        interactive: true,
        
        // popupContent [function]
        //   custom popup content
        //   function takes array of feature properties for all hazards at map-click point as input and must return html string of content to display in popup
        //   has no effect when "interactive" layer option set false (popups disabled)
        //   omit or set undefined to use built-in popup content
        //   example:
        //        popupContent: function(items){
        //            var html = "";
        //            $.each( items, function(idx,item){
        //                html += '<hr/>'
        //                    + '<h4>'+ item.prod_type +'</h4>'
        //                    + '<b>Issued:  </b>'+ item.issued + '<br/>'
        //                    + '<b>Expires: </b>'+ item.expires;
        //            });
        //            return html;
        //        }
        popupContent: undefined,
        
        //...............
        // event callbacks
        //...............
        
        // onSuccess [function]
        //   function to execute after layer is successfully created
        //   function can take a single input argument to access the leaflet geojson layer
        //   useful for doing creation tasks that require the layer to be populated with data (such as zooming map to layer)
        //   default is to do nothing
        onSuccess: function(lyr){},

        // onRefresh  [function]
        //   function to execute after layer is refreshed (on creation and any subsequent auto or manual refreshes)
        //   function can take a single input argument to access the leaflet geojson layer
        //   useful for doing tasks that depend on the current layer state (such as updating page annotations like last refresh time and number of features)
        //   default is to do nothing
        onRefresh: function(lyr){},
        
        // onError [function]
        //   function to execute if layer not successfully created
        //   when an error occurs during creation, the layer contains no data and autoRefresh is disabled
        //   function can take 2 input arguments: 1st is the leaflet geojson layer and 2nd is the error message string
        //   default is to do nothing
        //   errors are logged as a console warning regardless of this option
        onError: function(lyr,msg){}
    };
    
    // merge user input with defaults
    options = L.Util.extend( $.extend({},defaultOptions), options );

    //.............................
    // create new, empty geojson layer
    var layer = L.geoJSON( null, {
        style: function(feature){
            // style according to type
            var className = "util-leaflet-nwsWatches  util-leaflet-nwsWatches-other";
            if      (   /warning/i.test( feature.properties.prod_type ) ){ className = "util-leaflet-nwsWatches  util-leaflet-nwsWatches-warning"  ; }
            else if (     /watch/i.test( feature.properties.prod_type ) ){ className = "util-leaflet-nwsWatches  util-leaflet-nwsWatches-watch"    ; }
            else if (  /advisory/i.test( feature.properties.prod_type ) ){ className = "util-leaflet-nwsWatches  util-leaflet-nwsWatches-advisory" ; }
            else if ( /statement/i.test( feature.properties.prod_type ) ){ className = "util-leaflet-nwsWatches  util-leaflet-nwsWatches-statement"; }
            return {
                stroke      : true,
                weight      : 2,
                opacity     : options.opacity,
                fill        : true,
                fillOpacity : options.opacity,
                interactive : options.interactive,
                className   : className
            };
        },
        onEachFeature: function(feature,layer){
            // add some custom properties
            feature.properties.issued  = Date.parse(layer.feature.properties.issuance  );  feature.properties.issued  = ( isNaN(feature.properties.issued ) ? "N/A" : new Date(feature.properties.issued ) );
            feature.properties.expires = Date.parse(layer.feature.properties.expiration);  feature.properties.expires = ( isNaN(feature.properties.expires) ? "N/A" : new Date(feature.properties.expires) );
            feature.properties.note    = "";
            if      ( /warning/i.test( layer.feature.properties.prod_type ) ){ feature.properties.note = 'A warning indicates that this event is imminent or occurring and may pose a risk to life and property.'; }
            else if (   /watch/i.test( layer.feature.properties.prod_type ) ){ feature.properties.note = 'A watch indicates that conditions are favorable for this event in and near the watch area (but not necessarily occurring) and may pose a risk to life and property.'; }
            else if (/advisory/i.test( layer.feature.properties.prod_type ) ){ feature.properties.note = 'An advisory indicates that this event is imminent or occurring and may lead to nuisance conditions but not necessarily pose a risk to life and property.'; }
            var expires_ms = ( feature.properties.expires==="N/A" ? NaN : feature.properties.expires.getTime() ) - (new Date()).getTime();
            var from_now;
            if     ( expires_ms/(1000*60)    < 60 ){ from_now = (expires_ms/(1000*60      )).toFixed(0)+" minutes from now"; }
            else if( expires_ms/(1000*60*60) < 24 ){ from_now = (expires_ms/(1000*60*60   )).toFixed(1)+" hours from now"  ; }
            else                                   { from_now = (expires_ms/(1000*60*60*24)).toFixed(1)+" days from now"   ; }
            feature.properties.expires_from_now = ( !isNaN(parseFloat(from_now)) && parseFloat(from_now)>0 ? from_now : "");
        }
    })
        .on( "click", function(e){
            
            // multiple features can overlap - find all that intersect click point
            var polygons = util.pointInGeoJson( e.latlng, this );
            if( polygons.length<=0 ){ return; }
            
            // order multiple by type
            var wrn=[];  var wtc=[];  var adv=[];  var stm=[];  var oth=[];
            $.each( polygons, function(idx,polygon){
                var p = polygon.feature.properties;
                if      (   /warning/i.test( p.prod_type ) ){ wrn.push(p); }
                else if (     /watch/i.test( p.prod_type ) ){ wtc.push(p); }
                else if (  /advisory/i.test( p.prod_type ) ){ adv.push(p); }
                else if ( /statement/i.test( p.prod_type ) ){ stm.push(p); }
                else                                        { oth.push(p); }
            });
            var items = wrn.concat( wtc, adv, stm, oth );

            // built in or user defined popup
            var html = "";
            if ( typeof options.popupContent !== "function" ){

                // built-in
                $.each( items, function(idx,item){
                    html += '\
                        <table> \
                            <tr> \
                                <td width="100"> \
                                    <div class="util-leaflet-popup-thumbnail  util-leaflet-popup-thumbnail-nwsWatches" onclick="util.popupWindow(\''+ item.url +'\', { noCache:true, width:700 });"> \
                                        <div></div> \
                                        View Weather Service Report \
                                    </div> \
                                </td> \
                                <td> \
                                    <h4 style="text-align:left; margin-bottom:5px;">'+ item.prod_type +'</h4> \
                                    <b>Expires '+ item.expires_from_now +'</b><br/>'+ item["expires"].toString().replace(" (","<br/>(") +'\
                                </td> \
                            </tr> \
                        </table>';
                    if( item.note!=="" ){
                        html += '<div class="alert alert-'+( /warning/i.test( item.prod_type ) ? "warning":"info" )+'">'+ item.note +'</div>';
                    }
                });
                html = '\
                    <div class="util-leaflet-popup"> \
                        <b>'+ polygons.length +' hazard'+ (polygons.length===1 ? "":"s") +'</b> \
                        <div style="max-height:300px; overflow-y:auto; margin-bottom:5px;">'+ html +'</div> \
                        <div style="text-align:center;"> \
                            <button type="button" class="btn btn-sm btn-danger" onclick="$(\'.leaflet-popup\').remove()"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Close </button> \
                        </div> \
                    </div>';
            } else {

                // user defined
                html = options.popupContent(items);
            }

            // show popup
            if( typeof html==="string" ) {
                this._map.openPopup( html, e.latlng, { minWidth:350 });
            }
        });

    // add options
    layer._options = options;
    
    //------------------------------
    // public methods
    //------------------------------
    
    // refresh layer with new service call
    layer.refresh = function(){
        // call nws ags and populate layer with data
        $.ajax({
            url : "https://idpgis.ncep.noaa.gov/arcgis/rest/services/NWS_Forecasts_Guidance_Warnings/watch_warn_adv/MapServer/1/query",
            data: {
                // ...query parameters...
                inSr         : 4326,
                where        : "1=1",
                spatialRel   : ( !layer._options.latLngBounds ? "" : "esriSpatialRelIntersects" ),
                geometryType : ( !layer._options.latLngBounds ? "" : "esriGeometryEnvelope" ),
                geometry     : ( !layer._options.latLngBounds ? "" : JSON.stringify({ ymin:layer._options.latLngBounds.getSouth(), ymax:layer._options.latLngBounds.getNorth(), xmin:layer._options.latLngBounds.getWest(), xmax:layer._options.latLngBounds.getEast(), spatialReference: {wkid:4326} }) ),
                // ...output options...
                outSr              : 4326,
                outFields          : "prod_type,issuance,expiration,url",
                returnGeometry     : true,
                maxAllowableOffset : 0.03,  // geometry simplification
                geometryPrecision  : 4,
                f                  : "json" // ags does no simplification for geojson option
            },
            type     : "GET",
            dataType : "json",
            async    : true,
            cache    : false,
            error: function(xhr){
                // log warning
                console.warn("[util_leaflet] L.nwsWatches service error: "+xhr.statusText);
                // trigger error
                if ( typeof layer._options.onError==="function" ) {
                    layer._options.onError( layer, xhr.statusText );
                }
                // clear refresh interval
                if(layer._refreshInterval){ clearInterval( layer._refreshInterval ); }
            },
            success: function( esriGeometryPolygon ){
                // convert esriGeometryPolygon to geojson
                var geojson = { type:"FeatureCollection" };
                var wrn=[];  var wtc=[];  var adv=[];  var stm=[];  var oth=[]; // set top-bottom order of features based on type
                $.each( esriGeometryPolygon.features, function(idx,feature){
                    var f = {
                        type     : "Feature",
                        geometry : {
                            type        : "MultiPolygon",
                            coordinates : []
                        },
                    };
                    $.each( feature.geometry.rings, function(idx,ring){
                        f.geometry.coordinates.push( [ring] );
                    });
                    f.properties = feature.attributes;
                    if      (   /warning/i.test( feature.attributes.prod_type ) ){ wrn.push(f); }
                    else if (     /watch/i.test( feature.attributes.prod_type ) ){ wtc.push(f); }
                    else if (  /advisory/i.test( feature.attributes.prod_type ) ){ adv.push(f); }
                    else if ( /statement/i.test( feature.attributes.prod_type ) ){ stm.push(f); }
                    else                                                         { oth.push(f); }
                });
                geojson.features = oth.concat( stm, adv, wtc, wrn ); // set top-bottom order
                // populate geojson layer with data
                layer.clearLayers().addData( geojson );
                // trigger and disconnect success
                if ( typeof layer._options.onSuccess==="function" ) {
                    layer._options.onSuccess( layer );
                    delete layer._options.onSuccess;
                }
                // trigger refresh
                if ( typeof layer._options.onRefresh==="function" ) {
                    layer._options.onRefresh( layer );
                }
            }
        });
        return layer;
    };
    
    // set opacity
    layer.setOpacity = function(opacity){
        // update and return layer
        return layer.setStyle({ opacity:opacity, fillOpacity:opacity });
    };
    
    //------------------------------
    // private
    //------------------------------
    
    // populate layer with data on creation
    layer.refresh();
    
    // on add to map
    layer.on("add", function(){
        // set auto-refresh interval if specified
        if ( layer._options.autoRefresh ) {
            layer._refreshInterval = setInterval( function(){
                layer.refresh();
            }, 5*60*1000 ); // 5 minutes
        }
    });
    
    // on remove from map
    layer.on("remove", function(){
        // clear auto-refresh interval
        if(layer._refreshInterval){ clearInterval( layer._refreshInterval ); }
    });
    
    // done
    return layer;
};


//===========================================================
// cwis-leaflet.js (version 1.0)
//===========================================================
// L.cwis
//    create L.geoJSON layer containing real-time USGS NWISWeb data.
//    returns the layer.
//
// USAGE:
//    L.cwis( <string> type, <object> serviceOptions?, <object> layerOptions? )
//
//    where type, serviceOptions, and layerOptions are defined in the default options section below.
//    if only 2 input arguments are provided the 2nd is assumed to be serviceOptions.
//
// EXAMPLE:
//    L.cwis( "site", {states:"or,wa,ca"} ).addTo(map);
//
// EXAMPLE:
//    var layer = L.cwis(
//        "site", // layer type
//        { // service options for data retrieval
//            siteNumbers: "08210000,08206910,08194500,08206600,08206700"
//        },
//        { // layer options for data presentation
//            shape     : "triangle",
//            radius    : 10,
//            fillColor : "aqua",
//            onSuccess : function(lyr){
//                map.fitBounds( lyr.getBounds() );
//            }
//        }
//    );
//    map.addLayer(layer);
//
// EXAMPLE: dynamically size markers based on zoom level
//    var layer = L.cwis( "flow", {states:"tx,nm"}, {radius:Math.min(Math.max(map.getZoom()-4,1),6)} ).addTo(map);
//    map.on("zoomend", function(){
//        layer.setRadius( Math.min(Math.max(map.getZoom()-4,1),6) );
//    });
//
// EXAMPLES:
//            type    serviceOptions                     layerOptions
//            ----    --------------                     ------------
//    L.cwis( "site", { minGageAltitude:5000 },          { shape:"triangle", fillColor:"pink" } ).addTo(map);  // sites at or above 5000ft in nation
//    L.cwis( "site", { parameterCodes:"00095,00300" }                                          ).addTo(map);  // sites that collect specific conductance and/or dissolved oxygen in nation
//    L.cwis( "flow", { minStageRateOfChange:1 }                                                ).addTo(map);  // streamgages where stage is rising 1 ft/hr or more in nation
//    L.cwis( "flow", { isMinorFloodStage:true },        { shape:"star", radius:15 }            ).addTo(map);  // streamgages above NWS flood stage in nation
//    L.cwis( "flow", { minFlowValue:0, maxFlowValue:0 }                                        ).addTo(map);  // streamgages at 0 flow in nation
//    L.cwis( "rain", { minRainValue:0.00001 }                                                  ).addTo(map);  // rain gages where it is raining in nation
//    L.cwis( "lake", {},                                { addUnranked:false }                  ).addTo(map);  // lakes that have statistical ranking in nation
//    L.cwis( "lake", { maxLakeRateOfChange:-0.05 }                                             ).addTo(map);  // lakes where elevation is falling 0.05 ft/hr or more in nation
//    L.cwis( "well", {},                                { addUnranked:false }                  ).addTo(map);  // wells that have statistical ranking in nation
//    L.cwis( "well", { minWellRateOfChange:1 }                                                 ).addTo(map);  // wells where depth-to-water is increasing 1 inch/hr or more (water level falling) in nation
//
// the layer returned has standard properties and methods of a L.geoJSON layer.
// these additional extended methods are also available:
//
//    layer.updateLayerOptions(options) [returns layer] update layer options and redraw, input options merged into existing options (useful for re-symbolizing layer without a new service call)
//    layer.resetLayerOptions(options)  [returns layer] reset  layer options and redraw, input options replace     existing options (useful for re-symbolizing layer without a new service call)
//    layer.setOpacity(opacity)         [returns layer] set layer opacity
//    layer.setRadius(radius)           [returns layer] set marker radius
//    layer.refresh()                   [returns layer] refresh layer with new service call
//
L.cwis = function( type, serviceOptions, layerOptions ) {
    
    //------------------------------
    // default options
    //------------------------------
    
    // layer type, one of:
    //   "site" - all real-time sites
    //   "flow" - real-time stream sites with gage height data
    //   "lake" - real-time lake sites with lake elevation data
    //   "well" - real-time groundwater sites with water level data
    //   "rain" - real-time sites with precipitation data
    //   "qw"   - real-time sites collecting water quality  data
    //   "met"  - real-time sites collecting meteorological data
    var defaultType = "site";
    
    // CWIS data service options
    var defaultServiceOptions = {
        
        // caller [string]
        //   tag to use for internal data service usage logging
        caller: "leaflet_plugin_1.0"
        
        // all CWIS data service url options supported
        // unrecognized service options result in "invalid query" error
    };
    
    // layer options
    // unrecognized options are ignored
    var defaultLayerOptions = {
        
        //...............
        // marker symbolization
        //...............
        
        // shape [string]
        //   marker shape, see L.ShapeMarker at bottom of this plugin for possible values
        //   if omitted or undefined a default based on layer type is used:
        //     site - circle
        //     flow - circle
        //     lake - square
        //     well - up triangle
        //     rain - diamond
        //     qw   - down triangle
        //     met  - pentagon
        shape: undefined,
        
        // radius [number greater than 0]
        //   marker size, in pixels
        radius: 6,

        // stroke [boolean]
        //   whether to draw an outline around the marker
        stroke: true,
        
        // weight [number greater than 0]
        //   width of outline marker, in pixels
        weight: 2,
        
        // color [string]
        //   marker outline color
        //   if omitted or undefined a default based on layer type is used:
        //     site           - gray outline
        //     flow,lake,well - day-of-year percentile ranking
        //     rain           - gray outline
        //     qw             - orange
        //     met            - dark cyan
        color: undefined,
        
        // opacity [number between 0 and 1 inclusive]
        //   marker outline opacity
        opacity: 1,
        
        // fillColor [string]
        //   marker fill color
        //   if omitted or undefined a default based on layer type is used:
        //     site           - white
        //     flow,lake,well - day-of-year percentile ranking
        //     rain           - shades of purple based on rainfall rate
        //     qw             - yellow
        //     met            - light cyan
        fillColor: undefined,
        
        // fillOpacity [number between 0 and 1 inclusive]
        //   marker fill opacity
        fillOpacity: 1,
        
        // className [string]
        //   custom class(es) to add to marker element
        //   separate multiple classes with spaces
        //   omit or set to empty string to not apply a custom class
        className: "",
        
        // mergeOptions [object or function returning object]
        //   object of L.shapeMarker options (see L.shapeMarker at bottom of this plugin) that is merged into above options
        //   can be a function that takes point properties as input and returns object of options
        //   have function return undefined or false to filter a point from being added to layer
        //   useful for custom symbolization based on point properties
        //   set to empty object to do nothing
        // example:
        //    mergeOptions: function(properties){
        //        // keep default flow symbolization but set colors based on flow magnitude
        //        var mag = Math.abs( parseFloat(properties.FlowValue) );
        //        if      ( isNaN(mag) ) { return false; } // flow unavailable - do not add
        //        if      (mag > 100000) { return{ color:"#009", fillColor:"#000099" }; } // >100,000
        //        else if (mag >  10000) { return{ color:"#009", fillColor:"#0000e6" }; } // 10,000 - 100,000
        //        else if (mag >   1000) { return{ color:"#009", fillColor:"#3333ff" }; } //  1,000 -  10,000
        //        else if (mag >    100) { return{ color:"#009", fillColor:"#8080ff" }; } //    100 -   1,000
        //        else if (mag >     10) { return{ color:"#009", fillColor:"#ccccff" }; } //     10 -     100
        //        else if (mag >      0) { return{ color:"#009", fillColor:"#e6e6ff" }; } //      0 -      10
        //        else                   { return{ color:"#c90", fillColor:"#ffffff" }; } //      0 (not flowing)
        //    }
        mergeOptions: {},
        
        //...............
        // popups
        // note: although bootstrap is not required, bootstrap components and classes are used in the built-in popup
        //...............
        
        // interactive [boolean]
        //   whether to enable marker mouse-click popup
        interactive: true,
        
        // popupClassName [string]
        //   custom class(es) to add to marker popup
        //   class(es) are applied to built-in and custom popups
        //   separate multiple classes with spaces
        //   omit or set to empty string to not apply a custom class
        popupClassName: "",
        
        // popupContent [function]
        //   custom popup content
        //   function takes feature properties as input and should return html string of content to display in popup
        //   has no effect when "interactive" layer option set false (popups disabled)
        //   omit or set undefined to use built-in popup content
        //   example:
        //        popupContent: function(properties){
        //            return "<h5>"+properties.SiteName+"</h5>";
        //        }
        popupContent: undefined,
        
        //...............
        // misc
        //...............
        
        // addUnranked [boolean]
        //   whether to include points that have no statistical ranking ("flow", "lake", and "well" layer types) or no rainfall ("rain" layer type)
        //   has no effect for "site" layer type
        addUnranked: true,
        
        // autoRefresh [boolean]
        //   whether to auto-refresh the layer every minute to update marker symbology and popup info with latest data available from NWIS
        //   has no effect for "site" layer type
        //   a layer will not refresh while one of its marker's popups is open
        autoRefresh: true,
        
        //...............
        // event callbacks
        //...............
        
        // onSuccess [function]
        //   function to execute after layer is successfully created and contains at least 1 point
        //   function can take a single input argument to access the leaflet geojson layer
        //   useful for doing creation tasks that require the layer to be populated with markers (such as zooming map to layer bounds)
        //   default is to do nothing
        onSuccess: function(lyr){},

        // onRefresh  [function]
        //   function to execute after layer is refreshed (on creation and any subsequent auto or manual refreshes)
        //   function can take a single input argument to access the leaflet geojson layer
        //   useful for doing tasks that depend on the current layer state (such as updating page annotations like last refresh time and number of points)
        //   default is to do nothing
        onRefresh: function(lyr){},
        
        // onError [function]
        //   function to execute if layer not successfully created
        //   non-success is any condition resulting in the layer containing no points, including:
        //     - no NWIS real-time sites exist meeting the input serviceOptions
        //     - the input serviceOptions are invalid
        //     - CWIS services are unavailable
        //   when an error occurs during creation, the layer contains no points and autoRefresh is disabled
        //   function can take 2 input arguments: 1st is the leaflet geojson layer and 2nd is the error message string
        //   default is to do nothing
        //   errors are logged as a console warning regardless of this option
        onError: function(lyr,msg){}
    };
    
    //.............................
    // create new, empty geojson layer
    var layer = new L.GeoJSON();
    
    // set options
    layer._type           = ( type ? type : defaultType );
    layer._serviceOptions = L.Util.extend( $.extend({},defaultServiceOptions), serviceOptions ); // merge user input with defaults
    layer._layerOptions   = L.Util.extend( $.extend({},defaultLayerOptions  ), layerOptions   ); // merge user input with defaults
    
    //------------------------------
    // public methods
    //------------------------------
    
    // update layer options and redraw
    layer.updateLayerOptions = function(layerOptions){
        // merge options
        layer._layerOptions = L.Util.extend( layer._layerOptions, layerOptions );
        
        // redraw
        if (layer._geojson) {
            layer.clearLayers();
            L.GeoJSON.prototype.initialize.call( layer, layer._geojson );
        } else {
            layer.refresh();
        }
        return layer;
    };

    // reset layer options and redraw
    layer.resetLayerOptions = function(layerOptions){
        // reset options
        layer._layerOptions = L.Util.extend( $.extend({},defaultLayerOptions), layerOptions );
        
        // redraw
        if (layer._geojson) {
            layer.clearLayers();
            L.GeoJSON.prototype.initialize.call( layer, layer._geojson );
        } else {
            layer.refresh();
        }
        return layer;
    };
    
    // refresh layer with new service call
    layer.refresh = function(){
        // NOTE: do not use jQuery ajax - jQuery not required to be loaded for this plugin to work
        
        // build service url
        layer._serviceOptions.service = layer._type; // set service type
        var parms = [ "_="+(new Date()).getTime() ]; // no cache timestamp
        for (var k in layer._serviceOptions) {
            parms.push( k+"="+layer._serviceOptions[k] );
        }
        var url = "https://txdata.usgs.gov/CWIS/Services/1.0/services/request.ashx/getData?" + parms.join("&");
        
        // error handler
        var error = function(err){
            // log warning
            console.warn("[cwis-leaflet] service error: "+err);
            // trigger error
            if ( typeof layer._layerOptions.onError==="function" ) {
                layer._layerOptions.onError( layer, err );
            }
            // clear refresh interval
            if(layer._refreshInterval){ clearInterval( layer._refreshInterval ); }
        };
        
        // make http request
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function(){
            if (xmlhttp.readyState === XMLHttpRequest.DONE ) {
                // clear layer
                layer.clearLayers();
                layer._geojson = undefined;
                // check status
                if (xmlhttp.status !== 200) { error("status "+xmlhttp.status); return; }
                // parse as json
                var geojson;
                try {
                    geojson = JSON.parse( xmlhttp.responseText );
                } catch(err) {
                    error("response not valid json ("+err.message+")");
                    return;
                }
                // check geojson
                if (geojson.error) { error(geojson.error); return; }
                // add points
                layer._geojson = geojson;
                L.GeoJSON.prototype.initialize.call( layer, geojson );
                // trigger and disconnect success
                if ( typeof layer._layerOptions.onSuccess==="function" ) {
                    layer._layerOptions.onSuccess( layer );
                    delete layer._layerOptions.onSuccess;
                }
                // trigger refresh
                if ( typeof layer._layerOptions.onRefresh==="function" ) {
                    layer._layerOptions.onRefresh( layer );
                }
            }
        };
        xmlhttp.open("GET", url, true);
        xmlhttp.send();
        return layer;
    };
    
    // set opacity
    // preserves stroke-fill opacity ratio and resets options for layer refreshes
    layer.setOpacity = function(opacity){
        // set original stroke/fill ratio
        if (layer._opacityRatio===undefined) {
            layer._opacityRatio = layer._layerOptions.opacity / (layer._layerOptions.fillOpacity+0.0001);
        }
        // set new values preserving ratio
        var O,F;
        if (layer._opacityRatio > 1) {
            O = opacity;
            F = opacity / layer._opacityRatio;
        } else {
            F = opacity;
            O = opacity / layer._opacityRatio;
        }
        // reset options for layer refreshes
        layer._layerOptions.opacity     = O;
        layer._layerOptions.fillOpacity = F;
        // update and return layer
        return layer.setStyle({ opacity:O, fillOpacity:F });
    };
    
    // set marker radius
    // resets option for layer refreshes
    layer.setRadius = function(radius){
        layer._layerOptions.radius = radius;
        return layer.setStyle({ radius:radius });
    };
    
    //------------------------------
    // private
    //------------------------------
    
    // symbolization
    // ...flow, lake, well: color lookup by day-of-year percentile classification...
    var getRankColor = function(rank){
        if      (rank==="All-Time Low for this Day-of-Year"     ){ return{ color:"#900", fillColor:"#f00" }; } // red
        else if (rank==="Much Below Normal for this Day-of-Year"){ return{ color:"#611", fillColor:"#b22" }; } // maroon
        else if (rank==="Below Normal for this Day-of-Year"     ){ return{ color:"#960", fillColor:"#fa0" }; } // orange
        else if (rank==="Normal for this Day-of-Year"           ){ return{ color:"#090", fillColor:"#0f0" }; } // green
        else if (rank==="Above Normal for this Day-of-Year"     ){ return{ color:"#1aa", fillColor:"#4dd" }; } // aqua
        else if (rank==="Much Above Normal for this Day-of-Year"){ return{ color:"#009", fillColor:"#00f" }; } // blue
        else if (rank==="All-Time High for this Day-of-Year"    ){ return{ color:"#000", fillColor:"#005" }; } // black
        else                                                     { return{ color:"#bbb", fillColor:"#fff" }; } // light gray
    };
    // ...rain: color lookup by rain rate classification...
    var getRainColor = function(rank){
        if      (rank==="None"    ){ return{ color:"#bbb", fillColor:"#fff" }; } // light gray
        else if (rank==="Light"   ){ return{ color:"#bad", fillColor:"#dde" }; } // lt.purple
        else if (rank==="Moderate"){ return{ color:"#87a", fillColor:"#bad" }; } // ...darker...
        else if (rank==="Heavy"   ){ return{ color:"#528", fillColor:"#87a" }; } // ...darker...
        else if (rank==="Violent" ){ return{ color:"#204", fillColor:"#528" }; } // dk.purple
        else if (rank==="Extreme" ){ return{ color:"#b24", fillColor:"#f00" }; } // red
        else                       { return{ color:"#777", fillColor:"#ddd" }; } // gray
    };
    // ...get marker based on feature properties, latlng, and type...
    var getMarker = function(p,ll,t){
        
        // get any custom options to merge
        var mergeOptions = ( typeof layer._layerOptions.mergeOptions==="function" ? layer._layerOptions.mergeOptions(p) : layer._layerOptions.mergeOptions );
        if(!mergeOptions){ return undefined; } // do not add point
        
        // set default shape, color, and fill
        var shape, color, fillColor;
        switch(t) {
            case "site":
                shape     = "circle";
                color     = "#555";
                fillColor = "#fff";
                break;
            case "flow":
                shape     = "circle";
                color     = getRankColor( p["FlowClassification"] ).color;
                fillColor = getRankColor( p["FlowClassification"] ).fillColor;
                if (parseFloat(p.FlowValue)  ===0    ) { color="#970"; fillColor="#fff"; } // no flow
                if (p["AboveFloodStageMinor"]==="Yes") { color="#f49"; fillColor="#fff"; } // above flood stage
                break;
            case "lake":
                shape     = "square";
                color     = getRankColor( p["ElevClassification"] ).color;
                fillColor = getRankColor( p["ElevClassification"] ).fillColor;
                break;
            case "well":
                shape     = "triangleu";
                color     = getRankColor( p["WellClassification"] ).color;
                fillColor = getRankColor( p["WellClassification"] ).fillColor;
                break;
            case "rain":
                shape     = "diamond";
                color     = getRainColor( p["RainClassification"] ).color;
                fillColor = getRainColor( p["RainClassification"] ).fillColor;
                break;
            case "qw":
                shape     = "triangled";
                color     = "#c80";
                fillColor = "#ff9";
                break;
            case "met":
                shape     = "pentagon";
                color     = "#088";
                fillColor = "#cff";
                break;
        }
        
        // return marker with any custom options merged
        return L.shapeMarker( ll, L.Util.extend({
            shape       : ( typeof layer._layerOptions.shape    ==="string" ? layer._layerOptions.shape     : shape     ),
            color       : ( typeof layer._layerOptions.color    ==="string" ? layer._layerOptions.color     : color     ),
            fillColor   : ( typeof layer._layerOptions.fillColor==="string" ? layer._layerOptions.fillColor : fillColor ),
            radius      : layer._layerOptions.radius,
            stroke      : layer._layerOptions.stroke,
            weight      : layer._layerOptions.weight,
            opacity     : layer._layerOptions.opacity,
            fillOpacity : layer._layerOptions.fillOpacity,
            className   : layer._layerOptions.className,
            interactive : layer._layerOptions.interactive
        }, mergeOptions));
    };
    
    // built-in popup content
    // input is feature properties
    var popupContent = function(p){
        // content
        var html = $('\
            <div> \
                <div class="util-leaflet-popup"> \
                    <h4> \
                        '+ p.SiteNumber+' '+p.SiteName +'<br/> \
                        <small>'+ p.SiteTypeGroup + ' Site (' + p.SiteTypeName +')</small> \
                    </h4> \
                    <div class="insert-data"></div> \
                    <div class="btn-group btn-group-justified"> \
                        <div class="btn-group" '+( typeof $().modal==="function" ? '' : 'style="display: inline-block;"' )+'> <button type="button" class="btn btn-sm btn-default" onclick="window.open( \'https://waterdata.usgs.gov/nwis/uv?site_no='                                      +p.SiteNumber+'\', \'_blank\' )"> <span class="glyphicon glyphicon-new-window" aria-hidden="true"></span> More Data </button> </div> \
                        _SUBSCRIBE_ \
                        <div class="btn-group" '+( typeof $().modal==="function" ? '' : 'style="display: inline-block;"' )+'> <button type="button" class="btn btn-sm btn-danger"  onclick="$(\'.leaflet-popup\').remove()"                                                                                                  > <span class="glyphicon glyphicon-remove"     aria-hidden="true"></span> Close     </button> </div> \
                    </div> \
                </div> \
            </div> \
        ');
        // data section template
        var plotUrl = "https://txpub.usgs.gov/dss/gwis/0.0/services/plot?range_selector=true&date_window=15&controls=full_screen,full_range,y_fixscale,y_logscale,y_show0,x_grid,y_grid&y_fixscale=true&color=steelblue&period=p7d&title="+p.SiteNumber+"%20"+p.SiteName+"&site="+p.SiteNumber+"_GWISOPTS_";
        var dashUrl = "https://txpub.usgs.gov/dss/gwis/0.0/services/site?site="+p.SiteNumber+"&map=false&open=all";
        var plotWH  =
            "width=" + Math.floor( 0.5*window.screen.width  ) +","+
            "height="+ Math.floor( 0.3*window.screen.height ) +","+
            "left="  + Math.floor( (window.screen.width /2) - (0.5*window.screen.width /2) ) +","+
            "top="   + Math.floor( (window.screen.height/2) - (0.3*window.screen.height/2) );
        var dashWH  =
            "width=" + Math.floor( Math.max( 0.2*window.screen.width, 700 )  ) +","+
            "height="+ Math.floor( 0.8*window.screen.height ) +","+
            "left="  + Math.floor( (window.screen.width /2) - 300 ) +","+
            "top="   + 50;
        var dataTemplate = '\
            <table> \
                <tr> \
                    <td width="100"> \
                        <div class="util-leaflet-popup-thumbnail  util-leaflet-popup-thumbnail-cwis" onclick="\
                            var win = window.open( \'_URL_\', \'_blank\', \'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,copyhistory=no,_WH_\'); \
                            win.focus(); \
                        "> \
                            <div></div> \
                            View Graph \
                        </div> \
                    </td> \
                    <td> _INFO_ </td> \
                </tr> \
            </table>';
            
        // add data sections
        // ...flow...
        var roc, info, type_cd;
        if (p.FlowValue!==undefined && p.FlowValue!=="N/A") {
            type_cd = "sw";
            html.find(".insert-data").append(
                dataTemplate
                    .replace( "_INFO_", [
                            '<b>Real-Time Streamflow</b>',
                            p.FlowValue+' cubic feet per second',
                            p.FlowTime,
                            '<div class="alert alert-info">'+p.FlowClassification+'<\div>'
                    ].join('<br/>') )
                    .replace( "_WH_",  plotWH )
                    .replace( "_URL_", plotUrl
                        .replace( "_GWISOPTS_", "&pcode=00060&label=Streamflow&ylabel=Streamflow%2C%20cubic%20feet%20per%20second" )
                    )
            );
        }
        if (p.StageValue!==undefined && p.StageValue!=="N/A") {
            type_cd = "sw";
            roc = parseFloat( p.StageRateOfChange );
            if     (roc>0){ info = '<div class="alert alert-info"> Increasing '+ Math.abs(roc) +' feet per hour </div>'; }
            else if(roc<0){ info = '<div class="alert alert-info"> Decreasing '+ Math.abs(roc) +' feet per hour </div>'; }
            else          { info = '<div class="alert alert-info"> Remaining steady                             </div>'; }
            html.find(".insert-data").append(
                dataTemplate
                    .replace( "_INFO_", [
                            '<b>Real-Time Stream Stage</b>',
                            p.StageValue+' feet',
                            p.StageTime,
                            info
                    ].join('<br/>') )
                    .replace( "_WH_",  plotWH )
                    .replace( "_URL_", plotUrl
                        .replace( "_GWISOPTS_", "&pcode=00065&label=Stream%20Stage&ylabel=Stream%20Stage%2C%20feet" )
                    )
            );
            if( p.AboveFloodStageMinor==="Yes" ){
                html.find(".insert-data").append('\
                    <div class="alert alert-warning"> \
                        Currently above the National Weather Service flood stage of '+ p.FloodStageMinor +' feet \
                    </div> \
                ');
            }
        }
        // ...lake...
        if (p.ElevValue!==undefined && p.ElevValue!=="N/A") {
            type_cd = "sw";
            roc = parseFloat( p.ElevRateOfChange );
            if     (roc>0){ info = '<div class="alert alert-info"> Increasing '+ Math.abs(roc) +' feet per hour </div>'; }
            else if(roc<0){ info = '<div class="alert alert-info"> Decreasing '+ Math.abs(roc) +' feet per hour </div>'; }
            else          { info = '<div class="alert alert-info"> Remaining steady                             </div>'; }
            info += '<div class="alert alert-info">'+ p.ElevClassification +'</div>';
            html.find(".insert-data").append(
                dataTemplate
                    .replace( "_INFO_", [
                            '<b>Real-Time Lake Elevation</b>',
                            p.ElevValue+' feet',
                            p.ElevTime,
                            info
                    ].join('<br/>') )
                    .replace( "_WH_",  plotWH )
                    .replace( "_URL_", plotUrl
                        .replace( "_GWISOPTS_", "&pcode=62614&label=Lake%20Elevation&ylabel=Lake%20Elevation%2C%20feet" )
                    )
            );
        }
        // ...well...
        if (p.WellValue!==undefined && p.WellValue!=="N/A") {
            type_cd = "gw";
            roc = parseFloat( p.WellRateOfChange );
            if     (roc>0){ info = '<div class="alert alert-info"> Increasing '+ Math.abs(roc) +' inches per hour </div>'; }
            else if(roc<0){ info = '<div class="alert alert-info"> Decreasing '+ Math.abs(roc) +' inches per hour </div>'; }
            else          { info = '<div class="alert alert-info"> Remaining steady                               </div>'; }
            info += '<div class="alert alert-info">'+ p.WellClassification +'</div>';
            html.find(".insert-data").append(
                dataTemplate
                    .replace( "_INFO_", [
                            '<b>Real-Time Well Level</b>',
                            p.WellValue+' feet below land surface',
                            p.WellTime,
                            info
                    ].join('<br/>') )
                    .replace( "_WH_",  plotWH )
                    .replace( "_URL_", plotUrl
                        .replace( "_GWISOPTS_", "&pcode=72019&label=Well%20Depth&ylabel=Well%20Depth%2C%20feet%20below%20land%20surface" )
                    )
            );
        }
        // ...rain...
        if (p.RainRate!==undefined && p.RainRate!=="N/A") {
            type_cd = "at";
            html.find(".insert-data").append(
                dataTemplate
                    .replace( "_INFO_", [
                            '<b>Real-Time Rainfall</b>',
                            p.RainClassification,
                            p.RainRate+' inches per hour',
                            p.RainTime
                    ].join('<br/>') )
                    .replace( "_WH_",  plotWH )
                    .replace( "_URL_", plotUrl
                        .replace( "_GWISOPTS_", "&pcode=00045&label=Rainfall&ylabel=Rainfall%2C%20inches" )
                    )
            );
        }
        // ...qw...
        if ( layer._type==="qw" ) {
            type_cd = "qw";
            html.find(".insert-data").append(
                dataTemplate
                    .replace( "_INFO_", [
                            '<b>Real-Time Station Collecting Water Quality Data</b>',
                            p.County +", "+ p.State
                    ].join('<br/>') )
                    .replace( "_WH_",  dashWH  )
                    .replace( "_URL_", dashUrl )
            );
        }
        // ...met...
        if ( layer._type==="met" ) {
            type_cd = "at";
            html.find(".insert-data").append(
                dataTemplate
                    .replace( "_INFO_", [
                            '<b>Real-Time Station Collecting Meteorological Data</b>',
                            p.County +", "+ p.State
                    ].join('<br/>') )
                    .replace( "_WH_",  dashWH  )
                    .replace( "_URL_", dashUrl )
            );
        }
        
        // water alert subscribe requires type_cd
        // set "subscribe" or "more info" button based on type_cd
        html = html.html();
        if( type_cd !== undefined ){ html = html.replace( "_SUBSCRIBE_", '<div class="btn-group" '+( typeof $().modal==="function" ? '' : 'style="display: inline-block;"' )+'> <button type="button" class="btn btn-sm btn-success" onclick="window.open( \'https://water.usgs.gov/wateralert/subscribe2/index.html?type_cd='+type_cd+'&site_no='+p.SiteNumber+'\', \'_blank\' )"> <span class="glyphicon glyphicon-new-window" aria-hidden="true"></span> Subscribe </button> </div>' ); }
        else                       { html = html.replace( "_SUBSCRIBE_", '<div class="btn-group" '+( typeof $().modal==="function" ? '' : 'style="display: inline-block;"' )+'> <button type="button" class="btn btn-sm btn-default" onclick="window.open( \'https://waterdata.usgs.gov/nwis/inventory?site_no='                                  +p.SiteNumber+'\', \'_blank\' )"> <span class="glyphicon glyphicon-new-window" aria-hidden="true"></span> More Info </button> </div>' ); }
        
        // return html
        return html;
    };
    
    // marker symbolization
    layer.options.pointToLayer = function(feature,latlng){
        
        // skip unranked if specified
        if ( !layer._layerOptions.addUnranked && (                           // do not add unranked and...
            (/^not ranked/i).test(feature.properties.FlowClassification) ||  // ...unranked flow  or
            (/^not ranked/i).test(feature.properties.ElevClassification) ||  // ...unranked lake  or
            (/^not ranked/i).test(feature.properties.WellClassification) ||  // ...unranked well  or
            (/^none/i      ).test(feature.properties.RainClassification)     // ...no rainfall
            
        )) { return undefined; } // skip
        
        // set marker
        var marker = getMarker( feature.properties, latlng, layer._type );
        
        // bind custom or built-in popup if interactive
        if (marker && layer._layerOptions.interactive) {
            marker
                .bindPopup(
                    (typeof layer._layerOptions.popupContent ==="function" ?
                        '<div class="'+layer._layerOptions.popupClassName+'">'+layer._layerOptions.popupContent(feature.properties)+'</div>' : // custom or...
                        popupContent(feature.properties)                                                                                       // built-in
                    )
                )
                .on("click", function(){
                    this.openPopup(latlng); // open on marker lat-lon so anchored correctly when map zoomed in
                });
        }
        return marker;
    };
    
    // populate layer with data on creation
    layer.refresh();
    
    // on add to map
    layer.on("add", function(){
        // set auto-refresh interval if specified and not site layer
        if ( layer._layerOptions.autoRefresh && layer._type!=="site" ) {
            layer._refreshInterval = setInterval( function(){
                // see if popup open
                var isPopupOpen = false;
                layer.eachLayer( function(lyr){
                    if (lyr.isPopupOpen()) { isPopupOpen = true; };
                });
                // refresh if popup not open
                if (!isPopupOpen) { layer.refresh(); }
            }, 60*1000 ); // 1 minute
        }
    });
    
    // on remove from map
    layer.on("remove", function(){
        // clear auto-refresh interval
        if(layer._refreshInterval){ clearInterval( layer._refreshInterval ); }
    });
    
    // done
    return layer;
};


//===========================================================
// GEOMETRY UTILS
//===========================================================

//-----------------------------------------------------------
// pointInGeoJson
//   return  polygons in geojson that intersect a point
//
// USAGE:
//    util.pointInGeoJson( <L.LatLng> point, <L.GeoJSON> geojson, <boolean> firstOnly? )
//
//    point     - leaflet point or [lat,lng] array
//    geojson   - leaflet geojson layer
//    firstOnly - (optional) when true, only returns the 1st intersection sublayer found,  default is false (return all)
//
// RETURNS:
//    array of L.Polygon objects in the geojson layer that contain the point
//    empty array if none
//
// EXAMPLE:
//    var geojson  = L.geoJson( data );
//    var polygons = util.pointInGeoJson( [40,-90], geojson );
//    console.warn( polygons.length + " polygons found in geojson layer intersecting the point:" );
//    $.each( polygons, function(idx,polygon){
//        console.warn( polygon.feature.properties );
//    });
//
// adapted from leaflet-pip: https://github.com/mapbox/leaflet-pip
// core algorithm is PNPOLY from: https://wrf.ecse.rpi.edu//Research/Short_Notes/pnpoly.html
//
util.pointInGeoJson = function( point, geojson, firstOnly ){
    point = L.latLng( point );

    // function to determine if point is in polygon
    var pointInPolygon = function ( point, geometry ) {
        var coords = ( geometry.type==="Polygon" ? [ geometry.coordinates ] : geometry.coordinates );
        var inside = false;
        for(var n=0; n<coords.length; n++){
            // core PNPOLY algorithm
            var v = [[0,0]];
            for(var i=0; i<coords[n].length; i++){
                for(var j=0; j<coords[n][i].length; j++){
                    v.push(coords[n][i][j]);
                }
                v.push(coords[n][i][0]);
                v.push([0,0]);
            }
            var inside = false;
            for(var i=0, j=v.length-1; i<v.length; j=i++){
                if(
                    ((v[i][0] > point.lng) != (v[j][0] > point.lng)) &&
                    ( point.lat < (v[j][1] - v[i][1]) * (point.lng - v[i][0]) / (v[j][0] - v[i][0]) + v[i][1])
                ){
                    inside = !inside;
                }
            }
            if(inside){ break; }
        }
        return inside;
    };

    // loop through geojson layers
    var polygons = [];
    $.each( geojson.getLayers(), function(idx,lyr){
        if( firstOnly===true && polygons.length>0 ){ return false; }
        if(
            // ...check polygons only...
            lyr.feature &&
            lyr.feature.geometry &&
            lyr.feature.geometry.type &&
            ( lyr.feature.geometry.type==="Polygon" || lyr.feature.geometry.type==="MultiPolygon" ) &&
            // ...check if point in bbox around poly 1st (quick)...
            lyr.getBounds().contains( point ) &&
            // ...check if in actual polygon (slower)...
            pointInPolygon( point, lyr.feature.geometry )
        ) {
            polygons.push(lyr);
        }
    });
    return polygons;

}; // pointInGeoJson


//===========================================================
// MISC
//===========================================================

//-----------------------------------------------------------
// linkMaps
//   link 2 maps so zoom-pan 1 zoom-pans the other and vice-versa
//   set optional "unlink" arg false to unlink maps that have been previously linked
util.linkMaps = function ( map1, map2, unlink ) {
    var funcName = "util [linkMaps]: ";
    
    // flag preventing feedback loop
    // eg: moving map1 links map2, but resulting map2 movement links map1, and so on
    var isLinking = false;
    
    // function to link map views
    function link(e) {
        if (isLinking) { return; }
        isLinking = true;
        if        (e.target === map1) {
            map2.setView( e.target.getCenter(), e.target.getZoom(), {"animate": false} );
        } else if (e.target === map2) {
            map1.setView( e.target.getCenter(), e.target.getZoom(), {"animate": false} );
        }
        isLinking = false;
    };
    
    // link or unlink
    if (unlink === false) {
        // unlink maps
        console.log(funcName + "un-linking maps");
        if (typeof map1._removeSync === "function") { map1._removeSync(); };
        if (typeof map2._removeSync === "function") { map2._removeSync(); };
    } else {
        // link maps
        console.log(funcName + "linking maps");
        map1.on("move", link); map1._removeSync = function() { map1.off("move", link); };
        map2.on("move", link); map2._removeSync = function() { map2.off("move", link); };
        map2.setView( map1.getCenter(), map1.getZoom(), {"animate": false} );
    }
    
}; // linkMaps


//-----------------------------------------------------------
// L.ShapeMarker
//   additional SVG markers for leaflet
//   extends the path class
//
// extension options:
//   shape  [string, default "circle"] shape, see below
//   radius [number, default 10      ] radius (half total width) of the svg marker in pixels
//   rotDeg [number, default 0       ] clockwise rotation angle to apply to shape, in degrees
//
// available shapes:
//   generalized base shapes:
//     "Ngon"   - N-sided regular polygon where N is an integer, eg: "5gon"   is a pentagon
//     "Nstar"  - N-pointed star          where N is an integer, eg: "5star"  is a 5-pointed star
//     "Ncross" - N-armed cross           where N is an integer, eg: "4cross" is a plus-shaped cross
//   supported polygon aliases:
//     "triangle", "triangleu" (up), "triangled" (down), "trianglel" (left), "triangler" (right), "square", "diamond", "pentagon", "hexagon", "heptagon", "octagon", "nonagon", "decagon"
//   supported star aliases:
//     "star" (5-pointed)
//   supported cross aliases:
//     "cross", "plus", "x", "asterisk"
//   a circle is drawn if none of the above
//
// example:
//    var square = L.shapeMarker([51.505, -0.09], {
//        shape: "square",
//        radius: 20
//    }).addTo(map);
//
// also accepts all leaflet Path options:
//    var diamond = L.shapeMarker([51.505, -0.09], {
//        shape: "diamond",
//        fillColor: "#ccc",
//        color: "black",
//        shape: "diamond",
//        radius: 200
//    }).addTo(map);
//
// additional methods:
//
//   .setLatLng(latlng) [returns this  ] set the position of a marker to a new location
//   .getLatLng()       [returns LatLng] return the current geographical position of the marker
//
//   .setRadius(radius) [returns this  ] sets the radius of a marker, in pixels
//   .getRadius()       [returns this  ] return the current radius of the marker in pixels
//
//   .setStyle(options) [returns this  ] change the appearance using the input Path options object
//
//   .toGeoJSON()       [returns Object] returns a GeoJSON representation of the marker (GeoJSON Point Feature)
//    
// adapted from: https://github.com/rowanwins/Leaflet.SvgShapeMarkers

// constructor
L.ShapeMarker = L.Path.extend({
    
    // creation options
    options: {
        fill    : true,
        shape   : "circle",
        radius  : 10,
        rotDeg  : 0
    },
    initialize: function (latlng, options) {
        L.setOptions(this, options);
        this._latlng = L.latLng(latlng);
        this._radius = this.options.radius;
    },
    
    // public methods
    setLatLng: function (latlng) {
        this._latlng = L.latLng(latlng);
        this.redraw();
        return this.fire("move", {latlng: this._latlng});
    },
    getLatLng: function () {
        return this._latlng;
    },
    setRadius: function (radius) {
        this.options.radius = this._radius = radius;
        return this.redraw();
    },
    getRadius: function () {
        return this._radius;
    },
    setStyle : function (options) {
        var radius = options && options.radius || this._radius;
        L.Path.prototype.setStyle.call(this, options);
        this.setRadius(radius);
        return this;
    },
    toGeoJSON: function () {
        return L.GeoJSON.getFeature(this, {
            type        : "Point",
            coordinates : L.GeoJSON.latLngToCoords(this.getLatLng())
        });
    },
    
    // private methods
    _project: function () {
        this._point = this._map.latLngToLayerPoint(this._latlng);
        this._updateBounds();
    },
    _updateBounds: function () {
        var r  = this._radius;
        var r2 = this._radiusY || r;
        var w  = this._clickTolerance();
        var p  = [r + w, r2 + w];
        this._pxBounds = new L.Bounds(this._point.subtract(p), this._point.add(p));
    },
    _update: function () {
        if (this._map) {
            this._updatePath();
        }
    },
    _updatePath: function () {
        this._renderer._updateShape(this);
    },
    _empty: function () {
        return this._size && !this._renderer._bounds.intersects(this._pxBounds);
    }
});

// constructor registration
L.shapeMarker = function (latlng, options) {
    return new L.ShapeMarker(latlng, options);
};

// extended svg shapes
L.SVG.include({
    _updateShape: function (layer) {
        
        // convert shape aliases to base shapes
        var shape  = layer.options.shape;
        var rotRad = 0;
        switch(shape.toLowerCase()) {
            // polygons
            case "triangle"  : shape= "3gon";                      break;
            case "triangleu" : shape= "3gon";                      break;
            case "triangled" : shape= "3gon";  rotRad=  Math.PI;   break;
            case "trianglel" : shape= "3gon";  rotRad=  Math.PI/2; break;
            case "triangler" : shape= "3gon";  rotRad=3*Math.PI/2; break;
            case "square"    : shape= "4gon";  rotRad=  Math.PI/4; break;
            case "diamond"   : shape= "4gon";                      break;
            case "pentagon"  : shape= "5gon";                      break;
            case "hexagon"   : shape= "6gon";                      break;
            case "heptagon"  : shape= "7gon";                      break;
            case "octagon"   : shape= "8gon";  rotRad=  Math.PI/8; break;
            case "nonagon"   : shape= "9gon";                      break;
            case "decagon"   : shape="10gon";                      break;
            // stars
            case "star"      : shape="5star";                      break;
            // crosses
            case "cross"     : shape="4cross";                     break;
            case "plus"      : shape="4cross";                     break;
            case "x"         : shape="4cross"; rotRad=  Math.PI/4; break;
            case "asterisk"  : shape="6cross";                     break;
        }
        
        // parse base shapes: 'Ngon', 'Nstar', 'Ncross'
        var N       = undefined;
        var isPoly  = false;
        var isStar  = false;
        var isCross = false;
        if ( /^[0-9]+gon$/i.test( shape ) ) {
            isPoly = true;
            N = parseFloat(shape);
        } else if ( /^[0-9]+star$/i.test( shape ) ) {
            isStar = true;
            N = 2*parseFloat(shape);
        } else if ( /^[0-9]+cross$/i.test( shape ) ) {
            isCross = true;
            N = parseFloat(shape);
        }
        
        // if shape not recognized set circle
        if (N===undefined) {
            this._updateCircle(layer);
            return;
        }
        
        // function to get next x-y point
        var userRotRad = -layer.options.rotDeg * Math.PI / 180.0;
        var nextXY = function(r,i){
            var a = Math.PI - i*2*Math.PI/N + rotRad + userRotRad;
            var x = r * Math.sin(a);
            var y = r * Math.cos(a);
            return (p.x + x) + "," + (p.y + y);
        };
        
        // build svg path
        var p = layer._point;    // marker center point
        var r = layer._radius;   // radius
        var d = "M"+nextXY(r,0); // svg path to set
        for (var i=1; i<=N; i++) {
            if (isPoly) {
                // polygon: line to next point
                d += " L"+nextXY(r,i);
            } else if (isStar) {
                // star: line to next point with alternating radius
                d += " L"+nextXY( ( i%2 ? 0.35*r : r ), i );
            } else if (isCross) {
                // cross: line to center then move to next point
                d += "L"+(p.x) + "," + (p.y) + "M"+nextXY(r,i);
            }
        }
        
        // set marker svg path
        this._setPath(layer, d);
    }
});


//===========================================================
// END
//===========================================================
console.log("[util_leaflet] loaded and ready");
